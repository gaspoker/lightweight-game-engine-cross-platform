#include "CollisionFilter.h"
#include <stdexcept>

CollisionFilter::CollisionFilter(int group, int category, int mask) : group(group), category(category), mask(mask) {}

CollisionFilter::CollisionFilter(const JSONObject& json) {
    try {
        group = json.getInt("group");
        category = json.getInt("category");
        mask = json.getInt("mask");
    } catch (const JSONException& e) {
        // Manejo de excepción en caso de error al analizar el JSON
        // Por ejemplo, lanzar una excepción std::runtime_error
        throw std::runtime_error("Error parsing JSON for CollisionFilter: " + std::string(e.what()));
    }
}
