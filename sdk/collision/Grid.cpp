#include "Grid.h"

Grid::Grid(Collider* collider) : collider(collider), detector(collider) {
    bucketWidth = 48;
    bucketHeight = 48;
}

void Grid::update(std::vector<Body>& bodies, bool forceUpdate) {
    int col, row;
    bool gridChanged = false;

    for (size_t i = 0; i < bodies.size(); ++i) {
        Body& body = bodies[i];

        if (body.isSleeping && !forceUpdate)
            continue;

        if (body.bounds.max.x < collider->bounds.min.x || body.bounds.min.x > collider->bounds.max.x ||
            body.bounds.max.y < collider->bounds.min.y || body.bounds.min.y > collider->bounds.max.y)
            continue;

        Region newRegion = getRegion(body);

        if (body.region == nullptr || !newRegion.id.equals(body.region->id) || forceUpdate) {
            if (body.region == nullptr || forceUpdate) {
                body.region = std::make_shared<Region>(newRegion);
            }

            Region unionRegion = regionUnion(newRegion, *body.region);

            for (col = unionRegion.startCol; col <= unionRegion.endCol; ++col) {
                for (row = unionRegion.startRow; row <= unionRegion.endRow; ++row) {
                    std::string bucketId = getBucketId(col, row);
                    auto& bucket = buckets[bucketId];

                    bool isInsideNewRegion = (col >= newRegion.startCol && col <= newRegion.endCol &&
                                              row >= newRegion.startRow && row <= newRegion.endRow);

                    bool isInsideOldRegion = (col >= body.region->startCol && col <= body.region->endCol &&
                                              row >= body.region->startRow && row <= body.region->endRow);

                    if (!isInsideNewRegion && isInsideOldRegion) {
                        if (bucket != nullptr)
                            bucketRemoveBody(bucket, body);
                    }

                    if (body.region == newRegion || (isInsideNewRegion && !isInsideOldRegion) || forceUpdate) {
                        if (bucket == nullptr) {
                            bucket = std::make_shared<std::vector<Body>>();
                        }
                        bucketAddBody(*bucket, body);
                    }
                }
            }

            body.region = std::make_shared<Region>(newRegion);
            gridChanged = true;
        }
    }

    if (gridChanged) {
        pairsList = createActivePairsList();
    }
}

void Grid::clear() {
    buckets.clear();
    pairs.clear();
    pairsList.clear();
}

Grid::Region Grid::regionUnion(const Region& regionA, const Region& regionB) {
    int startCol = std::min(regionA.startCol, regionB.startCol);
    int endCol = std::max(regionA.endCol, regionB.endCol);
    int startRow = std::min(regionA.startRow, regionB.startRow);
    int endRow = std::max(regionA.endRow, regionB.endRow);

    return {startCol, endCol, startRow, endRow};
}

Grid::Region Grid::getRegion(const Body& body) {
    Bounds bounds = body.bounds;

    int startCol = static_cast<int>(floor(bounds.min.x / bucketWidth));
    int endCol = static_cast<int>(floor(bounds.max.x / bucketWidth));
    int startRow = static_cast<int>(floor(bounds.min.y / bucketHeight));
    int endRow = static_cast<int>(floor(bounds.max.y / bucketHeight));

    return {startCol, endCol, startRow, endRow};
}

std::string Grid::getBucketId(int column, int row) {
    return "C" + std::to_string(column) + "R" + std::to_string(row);
}

void Grid::bucketAddBody(std::shared_ptr<std::vector<Body>>& bucket, const Body& body) {
    for (size_t i = 0; i < bucket->size(); ++i) {
        const Body& bodyB = (*bucket)[i];

        if (body.id == bodyB.id || (body.isStatic && bodyB.isStatic))
            continue;

        std::string pairId = Pair::id(body, bodyB);
        auto pair = pairs.find(pairId);

        if (pair != pairs.end()) {
            pair->second.count++;
        } else {
            pairs.emplace(pairId, Pair(body, bodyB, 1));
        }
    }

    bucket->push_back(body);
}

void Grid::bucketRemoveBody(std::shared_ptr<std::vector<Body>>& bucket, const Body& body) {
    bucket->erase(std::remove_if(bucket->begin(), bucket->end(), [&](const Body& b) {
        std::string pairId = Pair::id(body, b);
        auto pair = pairs.find(pairId);

        if (pair != pairs.end()) {
            pair->second.count--;
        }

        return b.id == body.id;
    }), bucket->end());
}

std::vector<Pair> Grid::createActivePairsList() {
    std::vector<Pair> activePairs;

    std::set<std::string> pairKeys;
    for (const auto& pair : pairs) {
        pairKeys.insert(pair.first);
    }

    for (const auto& key : pairKeys) {
        Pair& pair = pairs[key];

        if (pair.count > 0) {
            activePairs.push_back(pair);
        } else {
            pairs.erase(key);
        }
    }

    return activePairs;
}
