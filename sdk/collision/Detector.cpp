#include "Detector.h"

Detector::Detector(Collider* cm) : cm(cm) {}

std::vector<Collision> Detector::collisions(std::vector<Pair>& broadphasePairs) {
    std::vector<Collision> collisions;
    std::vector<Pair>& pairsTable = cm->pairs.table;

    for (size_t i = 0; i < broadphasePairs.size(); ++i) {
        Body* bodyA = broadphasePairs[i].bodyA;
        Body* bodyB = broadphasePairs[i].bodyB;

        if ((bodyA->isStatic || bodyA->isSleeping) && (bodyB->isStatic || bodyB->isSleeping))
            continue;

        if (!canCollide(bodyA->collisionFilter, bodyB->collisionFilter))
            continue;

        // MID PHASE
        if (Bounds::overlaps(bodyA->bounds, bodyB->bounds)) {
            for (size_t j = bodyA->parts.size() > 1 ? 1 : 0; j < bodyA->parts.size(); ++j) {
                Body* partA = bodyA->parts[j];

                for (size_t k = bodyB->parts.size() > 1 ? 1 : 0; k < bodyB->parts.size(); ++k) {
                    Body* partB = bodyB->parts[k];

                    if ((partA == bodyA && partB == bodyB) || Bounds::overlaps(partA->bounds, partB->bounds)) {
                        // find a previous collision we could reuse
                        std::string pairId = Pair::id(partA, partB);
                        Pair* pair = Pairs::getById(pairsTable, pairId);
                        Collision* previousCollision = pair != nullptr && pair->isActive ? &pair->collision : nullptr;

                        // NARROW PHASE
                        Collision collision = SAT::collides(partA, partB, previousCollision);

                        if (collision.collided) {
                            collisions.push_back(collision);
                        }
                    }
                }
            }
        }
    }

    return collisions;
}

bool Detector::canCollide(const CollisionFilter& filterA, const CollisionFilter& filterB) {
    if (filterA.group == filterB.group && filterA.group != 0)
        return filterA.group > 0;

    return (filterA.mask & filterB.category) != 0 && (filterB.mask & filterA.category) != 0;
}
