#include "Pairs.h"

Pairs::Pairs() {
    this->table = std::vector<Pair>();
    this->list = std::vector<Pair>();
    this->collisionStart = std::vector<Pair>();
    this->collisionActive = std::vector<Pair>();
    this->collisionEnd = std::vector<Pair>();
}

void Pairs::update(std::vector<Collision>& collisions, long timestamp) {
    collisionStart.clear();
    collisionEnd.clear();
    collisionActive.clear();

    for (size_t i = 0; i < list.size(); i++) {
        list[i].confirmedActive = false;
    }

    for (size_t i = 0; i < collisions.size(); i++) {
        Collision& collision = collisions[i];

        if (collision.collided) {
            std::string pairId = Pair::id(collision.bodyA, collision.bodyB);
            Pair* pair = getById(table, pairId);

            if (pair != nullptr) {
                if (pair->isActive) {
                    collisionActive.push_back(*pair);
                } else {
                    collisionStart.push_back(*pair);
                }

                pair->update(collision, timestamp);
                pair->confirmedActive = true;
            } else {
                pair = new Pair(collision, timestamp);
                setById(table, pairId, *pair);

                collisionStart.push_back(*pair);
                list.push_back(*pair);
            }
        }
    }

    for (size_t i = 0; i < list.size(); i++) {
        Pair& pair = list[i];

        if (pair.isActive && !pair.confirmedActive) {
            pair.setActive(false, timestamp);
            collisionEnd.push_back(pair);
        }
    }
}

void Pairs::removeOld(long timestamp) {
    std::vector<int> indexesToRemove;

    for (size_t i = 0; i < list.size(); i++) {
        Pair& pair = list[i];
        Collision& collision = pair.collision;

        if (collision.bodyA.isSleeping || collision.bodyB.isSleeping) {
            pair.timeUpdated = timestamp;
            continue;
        }

        if (timestamp - pair.timeUpdated > Pair::MAX_IDLE_LIFE) {
            indexesToRemove.push_back(i);
        }
    }

    for (size_t i = 0; i < indexesToRemove.size(); i++) {
        int pairIndex = indexesToRemove[i] - i;
        Pair& pair = list[pairIndex];
        auto it = std::find(table.begin(), table.end(), pair);
        if (it != table.end()) table.erase(it);
        list.erase(list.begin() + pairIndex);
    }
}

Pairs Pairs::clear() {
    this->table.clear();
    this->list.clear();
    this->collisionStart.clear();
    this->collisionActive.clear();
    this->collisionEnd.clear();
    return *this;
}

void Pairs::setById(std::vector<Pair>& pairs, const std::string& id, Pair& newPair) {
    for (size_t i = 0; i < pairs.size(); i++) {
        Pair& pair = pairs[i];
        if (pair.id == id) {
            pair = newPair;
            break;
        }
    }
}

Pair* Pairs::getById(std::vector<Pair>& pairs, const std::string& id) {
    for (size_t i = 0; i < pairs.size(); i++) {
        Pair& pair = pairs[i];
        if (pair.id == id) {
            return &pair;
        }
    }
    return nullptr;
}

std::vector<Pair*> Pairs::getAllById(std::vector<Pair>& pairs, const std::string& id) {
    std::vector<Pair*> found;
    for (size_t i = 0; i < pairs.size(); i++) {
        Pair& pair = pairs[i];
        if (pair.id == id) {
            found.push_back(&pair);
        }
    }
    return found;
}
