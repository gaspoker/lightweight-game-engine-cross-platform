#pragma once

#include <algorithm>
#include <cmath>
#include <vector>

#include "core/Body.h"
#include "Collision.h"
#include "geometry/Vector.h"
#include "geometry/Vertex.h"
#include "geometry/Vertices.h"

class SAT {
public:
    static Collision collides(Body* bodyA, Body* bodyB, Collision* previousCollision = nullptr) {
        Overlap overlapAB, overlapBA, minOverlap;
        Collision collision;

        bool canReusePrevCol = false;
        if (previousCollision != nullptr) {
            // estimate total motion
            Body* parentA = bodyA->parent;
            Body* parentB = bodyB->parent;
            float motion = parentA->speed * parentA->speed + parentA->angularSpeed * parentA->angularSpeed +
                           parentB->speed * parentB->speed + parentB->angularSpeed * parentB->angularSpeed;

            // we may be able to (partially) reuse collision result
            // but only safe if collision was resting
            canReusePrevCol = previousCollision->collided && motion < 0.2;

            // reuse collision object
            collision = *previousCollision;
        } else {
            collision = Collision(bodyA, bodyB, false);
        }

        if (previousCollision != nullptr && canReusePrevCol) {
            // if we can reuse the collision result
            // we only need to test the previously found axis
            Body* axisBodyA = collision.axisBody;
            Body* axisBodyB = axisBodyA == bodyA ? bodyB : bodyA;
            std::vector<Vector> axes = {axisBodyA->axes[previousCollision->axisNumber]};

            minOverlap = overlapAxes(axisBodyA->vertices, axisBodyB->vertices, axes);
            collision.reused = true;

            if (minOverlap.overlap <= 0) {
                collision.collided = false;
                return collision;
            }
        } else {
            // if we can't reuse a result, perform a full SAT test
            overlapAB = overlapAxes(bodyA->vertices, bodyB->vertices, bodyA->axes);

            if (overlapAB.overlap <= 0) {
                collision.collided = false;
                return collision;
            }

            overlapBA = overlapAxes(bodyB->vertices, bodyA->vertices, bodyB->axes);

            if (overlapBA.overlap <= 0) {
                collision.collided = false;
                return collision;
            }

            if (overlapAB.overlap < overlapBA.overlap) {
                minOverlap = overlapAB;
                collision.axisBody = bodyA;
            } else {
                minOverlap = overlapBA;
                collision.axisBody = bodyB;
            }

            // important for reuse later
            collision.axisNumber = minOverlap.axisNumber;
        }

        collision.bodyA = bodyA->id < bodyB->id ? bodyA : bodyB;
        collision.bodyB = bodyA->id < bodyB->id ? bodyB : bodyA;
        collision.collided = true;
        collision.depth = minOverlap.overlap;
        collision.parentA = collision.bodyA->parent;
        collision.parentB = collision.bodyB->parent;

        bodyA = collision.bodyA;
        bodyB = collision.bodyB;

        // ensure normal is facing away from bodyA
        if (Vector::dot(minOverlap.axis, Vector::sub(bodyB->position, bodyA->position))) {
            collision.normal = Vector(minOverlap.axis.x, minOverlap.axis.y);
        } else {
            collision.normal = Vector(-minOverlap.axis.x, -minOverlap.axis.y);
        }

        collision.tangent = Vector::perp(collision.normal, false);

        collision.penetration.x = collision.normal.x * collision.depth;
        collision.penetration.y = collision.normal.y * collision.depth;

        // find support points, there is always either exactly one or two
        std::vector<Vertex*> verticesB = findSupports(bodyA, bodyB, collision.normal);
        std::vector<Vertex*> supports;

        // find the supports from bodyB that are inside bodyA
        if (Vertices::contains(bodyA->vertices, verticesB[0])) {
            supports.push_back(verticesB[0]);
        }

        if (supports.size() < 2 && Vertices::contains(bodyA->vertices, verticesB[1])) {
            supports.push_back(verticesB[1]);
        }

        // find the supports from bodyA that are inside bodyB
        if (supports.size() < 2) {
            std::vector<Vertex*> verticesA = findSupports(bodyB, bodyA, Vector::neg(collision.normal));

            if (Vertices::contains(bodyB->vertices, verticesA[0])) {
                supports.push_back(verticesA[0]);
            }

            if (supports.size() < 2 && Vertices::contains(bodyB->vertices, verticesA[1])) {
                supports.push_back(verticesA[1]);
            }
        }

        // account for the edge case of overlapping but no vertex containment
        if (supports.size() < 1) {
            supports.push_back(verticesB[0]);
        }

        collision.supports = supports;

        return collision;
    }

private:
    struct Overlap {
        int axisNumber;
        float overlap;
        Vector axis;

        Overlap() : axisNumber(0), overlap(std::numeric_limits<float>::max()) {}
    };

    static Overlap overlapAxes(const std::vector<Vertex*>& verticesA, const std::vector<Vertex*>& verticesB,
                               const std::vector<Vector>& axes) {
        Projection projectionA, projectionB;
        Overlap result;

        for (size_t i = 0; i < axes.size(); ++i) {
            Vector axis = axes[i];

            projectToAxis(projectionA, verticesA, axis);
            projectToAxis(projectionB, verticesB, axis);

            float overlap = std::min(projectionA.max - projectionB.min, projectionB.max - projectionA.min);

            if (overlap <= 0) {
                result.overlap = overlap;
                return result;
            }

            if (overlap < result.overlap) {
                result.overlap = overlap;
                result.axis = axis;
                result.axisNumber = static_cast<int>(i);
            }
        }

        return result;
    }

    static void projectToAxis(Projection& projection, const std::vector<Vertex*>& vertices, const Vector& axis) {
        float min = Vector::dot(*vertices[0], axis);
        float max = min;

        for (size_t i = 1; i < vertices.size(); ++i) {
            float dot = Vector::dot(*vertices[i], axis);

            if (dot > max) {
                max = dot;
            } else if (dot < min) {
                min = dot;
            }
        }

        projection.min = min;
        projection.max = max;
    }

    static std::vector<Vertex*> findSupports(Body* bodyA, Body* bodyB, const Vector& normal) {
        float nearestDistance = std::numeric_limits<float>::max();
        Vector vertexToBody;
        const std::vector<Vertex*>& vertices = bodyB->vertices;
        const Vector& bodyAPosition = bodyA->position;
        float distance;
        Vertex* vertexA = nullptr;
        Vertex* vertexB;

        // find closest vertex on bodyB
        for (size_t i = 0; i < vertices.size(); ++i) {
            Vertex* vertex = vertices[i];
            vertexToBody.x = vertex->x - bodyAPosition.x;
            vertexToBody.y = vertex->y - bodyAPosition.y;
            distance = -Vector::dot(normal, vertexToBody);

            if (distance < nearestDistance) {
                nearestDistance = distance;
                vertexA = vertex;
            }
        }

        // find next closest vertex using the two connected to it
        size_t prevIndex = (vertexA->index - 1 >= 0) ? vertexA->index - 1 : vertices.size() - 1;
        Vertex* vertex = vertices[prevIndex];
        vertexToBody.x = vertex->x - bodyAPosition.x;
        vertexToBody.y = vertex->y - bodyAPosition.y;
        nearestDistance = -Vector::dot(normal, vertexToBody);
        vertexB = vertex;

        size_t nextIndex = (vertexA->index + 1) % vertices.size();
        vertex = vertices[nextIndex];
        vertexToBody.x = vertex->x - bodyAPosition.x;
        vertexToBody.y = vertex->y - bodyAPosition.y;
        distance = -Vector::dot(normal, vertexToBody);
        if (distance < nearestDistance) {
            vertexB = vertex;
        }

        // Result
        std::vector<Vertex*> result = {vertexA, vertexB};
        return result;
    }

    struct Projection {
        float min, max;
    };
};
