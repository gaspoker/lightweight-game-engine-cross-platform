#include "Collider.h"

// Método para actualizar el estado del colisionador
void Collider::update(long timestamp, float deltaTime) {
    // Actualizar la posición y rotación de todos los cuerpos mediante la integración
    bodiesUpdate(deltaTime);

    // Si el mundo está sucio, debemos vaciar toda la cuadrícula
    // if (world.isModified)
    //     broadphase.controller.clear(broadphase);

    // Actualizar los cubos de la cuadrícula en función de los cuerpos actuales
    broadphase.update(bodies, false);

    // Pase de estrechamiento: encontrar colisiones reales, luego crear o actualizar pares de colisiones
    std::vector<Collision> collisions = broadphase.detector.collisions(broadphase.pairsList);

    // Actualizar pares de colisiones
    pairs.update(collisions, timestamp);
    pairs.removeOld(timestamp);

    // Desencadenar eventos de colisión
    if (!pairs.collisionStart.empty()) {
        collisionStart(pairs.collisionStart);
        // Events.trigger(engine, 'collisionStart', { pairs: pairs.collisionStart });
    }

    // Desencadenar eventos de colisión
    if (!pairs.collisionActive.empty()) {
        collisionActive(pairs.collisionActive);
        // Events.trigger(engine, 'collisionActive', { pairs: pairs.collisionActive });
    }

    if (!pairs.collisionEnd.empty()) {
        collisionEnd(pairs.collisionEnd);
        // Events.trigger(engine, 'collisionEnd', { pairs: pairs.collisionEnd });
    }
}

// Método para desencadenar el evento de inicio de colisión
void Collider::collisionStart(std::vector<Pair>& pairs) {
    if (listener != nullptr) {
        listener->onCollisionStart(pairs);
    }

    for (int i = 0; i < pairs.size(); i++) {
        Pair pair = pairs[i];
        Body* bodyA = pair.bodyA;
        Body* bodyB = pair.bodyB;

        bodyA->collisionStart(pair);
        bodyB->collisionStart(pair);
    }
}

// Método para desencadenar el evento de colisión activa
void Collider::collisionActive(std::vector<Pair>& pairs) {
    if (listener != nullptr) {
        listener->onCollisionActive(pairs);
    }

    for (int i = 0; i < pairs.size(); i++) {
        Pair pair = pairs[i];
        Body* bodyA = pair.bodyA;
        Body* bodyB = pair.bodyB;

        bodyA->collisionActive(pair);
        bodyB->collisionActive(pair);
    }
}

// Método para desencadenar el evento de fin de colisión
void Collider::collisionEnd(std::vector<Pair>& pairs) {
    if (listener != nullptr) {
        listener->onCollisionEnd(pairs);
    }

    for (int i = 0; i < pairs.size(); i++) {
        Pair pair = pairs[i];
        Body* bodyA = pair.bodyA;
        Body* bodyB = pair.bodyB;

        bodyA->collisionEnd(pair);
        bodyB->collisionEnd(pair);
    }
}

// Método para actualizar la posición y rotación de los cuerpos mediante la integración
void Collider::bodiesUpdate(float deltaTime) {
    for (int i = 0; i < bodies.size(); i++) {
        Body* body = bodies[i];

        if (body->isStatic || body->isSleeping)
            continue;

        body->update(deltaTime);
    }
}
