#ifndef PAIR_H
#define PAIR_H

#include "Body.h"
#include "Collision.h"
#include "Vertex.h"
#include <vector>
#include <string>

class Pair {
public:
    static const int MAX_IDLE_LIFE = 1000;

    Pair(Body& bodyA, Body& bodyB);
    Pair(Collision& collision, long timestamp);
    Pair(Body& bodyA, Body& bodyB, int count);

    void update(Collision& collision, long timestamp);
    void setActive(bool isActive, long timestamp);

    static std::string id(const Body& bodyA, const Body& bodyB);

    std::string id;
    Body& bodyA;
    Body& bodyB;
    std::vector<Vertex::Contact> activeContacts;
    float separation;
    bool isActive;
    bool confirmedActive;
    bool isSensor;
    long timeCreated;
    long timeUpdated;
    Collision collision;
    float inverseMass;
    float friction;
    float frictionStatic;
    float restitution;
    float slop;
    int count;
};

#endif // PAIR_H
