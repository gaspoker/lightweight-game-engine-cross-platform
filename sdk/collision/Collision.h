#ifndef COLLISION_H
#define COLLISION_H

#include "Body.h"
#include "Vector.h"
#include "Vertex.h"
#include <vector>

class Collision {
private:
    Body* axisBody;
    float depth;
    Body* bodyA;
    Body* bodyB;
    Body* parentA;
    Body* parentB;
    std::vector<Vertex> supports;
    Vector normal;
    Vector tangent;
    Vector penetration;
    bool reused;
    bool collided;
    int axisNumber;

public:
    Collision(Body* bodyA, Body* bodyB, bool collided);
};

#endif // COLLISION_H
