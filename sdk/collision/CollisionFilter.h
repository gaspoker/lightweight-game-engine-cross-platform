#ifndef COLLISION_FILTER_H
#define COLLISION_FILTER_H

#include "JSONObject.h"
#include "JSONException.h"

class CollisionFilter {
public:
    int group;
    int category;
    int mask;

    CollisionFilter(int group, int category, int mask);
    CollisionFilter(const JSONObject& json);
};

#endif // COLLISION_FILTER_H
