#ifndef SAT_H
#define SAT_H

#include "Collision.h"
#include "Body.h"
#include "Vector.h"
#include <vector>

class SAT {
public:
    static Collision collides(Body& bodyA, Body& bodyB, Collision& previousCollision);
    static std::vector<Vertex> findSupports(Body& bodyA, Body& bodyB, Vector& normal);

private:
    struct Overlap {
        int axisNumber;
        float overlap;
        Vector axis;

        Overlap() : overlap(INT_MAX) {}
    };

    struct Projection {
        float min;
        float max;
    };

    static Overlap overlapAxes(std::vector<Vertex>& verticesA, std::vector<Vertex>& verticesB, std::vector<Vector>& axes);
    static void projectToAxis(Projection& projection, std::vector<Vertex>& vertices, Vector& axis);
};

#endif // SAT_H
