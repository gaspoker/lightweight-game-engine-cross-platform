#ifndef DETECTOR_H
#define DETECTOR_H

#include "Collider.h"
#include "Pair.h"
#include "Pairs.h"
#include "SAT.h"
#include "Bounds.h"

#include <vector>

class Detector {
public:
    Collider* cm;

    Detector(Collider* cm);

    std::vector<Collision> collisions(std::vector<Pair>& broadphasePairs);

private:
    static bool canCollide(const CollisionFilter& filterA, const CollisionFilter& filterB);
};

#endif // DETECTOR_H
