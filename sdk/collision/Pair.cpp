#include "Pair.h"

Pair::Pair(Body& bodyA, Body& bodyB) 
    : bodyA(bodyA), bodyB(bodyB), id(id(bodyA, bodyB)), separation(0), isActive(true), confirmedActive(true), isSensor(bodyA.isSensor || bodyB.isSensor),
      timeCreated(0), timeUpdated(0), collision(nullptr), inverseMass(0), friction(0), frictionStatic(0), restitution(0), slop(0), count(0) {}

Pair::Pair(Collision& collision, long timestamp) 
    : Pair(collision.bodyA, collision.bodyB) {
    this->update(collision, timestamp);
}

Pair::Pair(Body& bodyA, Body& bodyB, int count) 
    : Pair(bodyA, bodyB) {
    this->count = count;
}

void Pair::update(Collision& collision, long timestamp) {
    this->collision = collision;

    if (collision.collided) {
        std::vector<Vertex>& supports = collision.supports;
        Body& parentA = collision.parentA;
        Body& parentB = collision.parentB;

        inverseMass = parentA.inverseMass + parentB.inverseMass;
        friction = std::min(parentA.friction, parentB.friction);
        frictionStatic = std::max(parentA.frictionStatic, parentB.frictionStatic);
        restitution = std::max(parentA.restitution, parentB.restitution);
        slop = std::max(parentA.slop, parentB.slop);

        for (size_t i = 0; i < supports.size(); i++) {
            activeContacts.push_back(supports[i].contact);
        }

        // optimize array size
        size_t supportCount = supports.size();
        if (supportCount < activeContacts.size()) {
            activeContacts.erase(activeContacts.begin() + supportCount, activeContacts.end());
        }

        separation = collision.depth;
        setActive(true, timestamp);
    } else {
        if (isActive) {
            setActive(false, timestamp);
        }
    }
}

void Pair::setActive(bool isActive, long timestamp) {
    if (isActive) {
        this->isActive = true;
        this->timeUpdated = timestamp;
    } else {
        this->isActive = false;
    }
}

std::string Pair::id(const Body& bodyA, const Body& bodyB) {
    if (bodyA.id < bodyB.id) {
        return "A" + std::to_string(bodyA.id) + "B" + std::to_string(bodyB.id);
    } else {
        return "A" + std::to_string(bodyB.id) + "B" + std::to_string(bodyA.id);
    }
}
