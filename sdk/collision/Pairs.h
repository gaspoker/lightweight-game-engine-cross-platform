#ifndef PAIRS_H
#define PAIRS_H

#include <vector>
#include <string>
#include "Collision.h"
#include "Pair.h"

class Pairs {
public:
    std::vector<Pair> table;
    std::vector<Pair> list;
    std::vector<Pair> collisionStart;
    std::vector<Pair> collisionActive;
    std::vector<Pair> collisionEnd;

    Pairs();

    void update(std::vector<Collision>& collisions, long timestamp);
    void removeOld(long timestamp);
    Pairs clear();

    static void setById(std::vector<Pair>& pairs, const std::string& id, Pair& newPair);
    static Pair* getById(std::vector<Pair>& pairs, const std::string& id);
    static std::vector<Pair*> getAllById(std::vector<Pair>& pairs, const std::string& id);
};

#endif // PAIRS_H
