#ifndef GRID_H
#define GRID_H

#include <vector>
#include <string>
#include <memory>
#include <algorithm>
#include <set>

#include "Collider.h"
#include "Pair.h"
#include "Body.h"
#include "geometry/Bounds.h"

class Grid {
public:
    struct Region {
        std::string id;
        int startCol, endCol, startRow, endRow;
    };

    Grid(Collider* collider);

    void update(std::vector<Body>& bodies, bool forceUpdate);
    void clear();

private:
    Collider* collider;
    Detector detector;
    std::map<std::string, std::shared_ptr<std::vector<Body>>> buckets;
    std::map<std::string, Pair> pairs;
    std::vector<Pair> pairsList;
    float bucketWidth;
    float bucketHeight;

    Region regionUnion(const Region& regionA, const Region& regionB);
    Region getRegion(const Body& body);
    std::string getBucketId(int column, int row);
    void bucketAddBody(std::shared_ptr<std::vector<Body>>& bucket, const Body& body);
    void bucketRemoveBody(std::s
}