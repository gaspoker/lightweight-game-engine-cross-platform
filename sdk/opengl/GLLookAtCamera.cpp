#include <GL/gl.h>
#include <GL/glu.h>

#include "geometry/Vector3D.h"

class GLLookAtCamera {
public:
    Vector3D position;
    Vector3D up;
    Vector3D lookAt;
    float fieldOfView;
    float aspectRatio;
    float near;
    float far;

    GLLookAtCamera(float fieldOfView, float aspectRatio, float near, float far) :
        fieldOfView(fieldOfView), aspectRatio(aspectRatio), near(near), far(far),
        position(Vector3D()), up(Vector3D(0, 1, 0)), lookAt(Vector3D(0, 0, -1)) {}

    Vector3D getPosition() {
        return position;
    }

    Vector3D getUp() {
        return up;
    }

    Vector3D getLookAt() {
        return lookAt;
    }

    void setMatrices() {
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        gluPerspective(fieldOfView, aspectRatio, near, far);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        gluLookAt(position.x, position.y, position.z, lookAt.x, lookAt.y, lookAt.z, up.x, up.y, up.z);
    }
};
