#include "GLGraphics.h"
#include "geometry/Vector.h"

class GLCamera2D {
private:
    Vector* position;
    float zoom;
    float frustumWidth;
    float frustumHeight;
    GLGraphics* glGraphics;
    bool inverseViewPort;

public:
    GLCamera2D(GLGraphics* glGraphics, float frustumWidth, float frustumHeight, bool inverseViewPort)
        : glGraphics(glGraphics), frustumWidth(frustumWidth), frustumHeight(frustumHeight), inverseViewPort(inverseViewPort) {
        this->position = new Vector(frustumWidth / 2, frustumHeight / 2);
        this->zoom = 1.0f;
    }

    void setViewportAndMatrices() {
        GL10* gl = glGraphics->getGL();
        gl->glViewport(0, 0, glGraphics->getWidth(), glGraphics->getHeight());
        gl->glMatrixMode(GL10.GL_PROJECTION);
        gl->glLoadIdentity();

        if (!inverseViewPort) {
            gl->glOrthof(position->x - frustumWidth * zoom / 2,
                    position->x + frustumWidth * zoom / 2,
                    position->y - frustumHeight * zoom / 2,
                    position->y + frustumHeight * zoom / 2,
                    1, -1);
        } else {
            // Left and Top = Zero coordinates
            gl->glOrthof(0, frustumWidth * zoom, frustumHeight * zoom, 0, 1, -1);
        }

        gl->glMatrixMode(GL10.GL_MODELVIEW);
        gl->glLoadIdentity();
    }

    void touchToWorld(Vector* touch) {
        if (!inverseViewPort) {
            touch->x = (touch->x / (float)glGraphics->getWidth()) * frustumWidth * zoom;
            touch->y = (1 - touch->y / (float)glGraphics->getHeight()) * frustumHeight * zoom;
        } else {
            // Left and Top = Zero coordinates
            touch->x = (touch->x / (float)glGraphics->getWidth()) * frustumWidth * zoom;
            touch->y = (touch->y / (float)glGraphics->getHeight()) * frustumHeight * zoom;
        }
        touch->add(position)->sub(frustumWidth * zoom / 2, frustumHeight * zoom / 2);
    }
};
