#include "GLTexture.h"
#include "GLTextureRegion.h"

class GLFont {
private:
    const GLTexture& texture;
    const int glyphWidth;
    const int glyphHeight;
    GLTextureRegion* glyphs[96];

public:
    GLFont(const GLTexture& texture, int offsetX, int offsetY, int glyphsPerRow, int glyphWidth, int glyphHeight)
        : texture(texture), glyphWidth(glyphWidth), glyphHeight(glyphHeight) {
        int x = offsetX;
        int y = offsetY;
        for (int i = 0; i < 96; i++) {
            glyphs[i] = new GLTextureRegion(texture, x, y, glyphWidth, glyphHeight);
            x += glyphWidth;
            if (x == offsetX + glyphsPerRow * glyphWidth) {
                x = offsetX;
                y += glyphHeight;
            }
        }
    }

    ~GLFont() {
        for (int i = 0; i < 96; i++) {
            delete glyphs[i];
        }
    }

    void drawText(GLSpriteBatcher& batcher, const std::string& text, float x, float y, int color, float alpha) {
        int len = text.length();
        for (int i = 0; i < len; i++) {
            int c = text[i] - ' ';
            if (c < 0 || c > 95) // Asumiendo que ' ' es el primer glifo y '~' es el último
                continue;

            GLTextureRegion* glyph = glyphs[c];
            batcher.drawSprite(x, y, glyphWidth, glyphHeight, *glyph, color, alpha);
            x += glyphWidth;
        }
    }
};
