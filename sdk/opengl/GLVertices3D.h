#ifndef GLVERTICES3D_H
#define GLVERTICES3D_H

#include "GLGraphics.h"

class GLVertices3D {
private:
    static const int BYTES_PER_FLOAT = 4;

    GLGraphics* glGraphics;
    bool hasColor;
    bool hasTexCoords;
    bool hasNormals;
    int vertexSize;
    int* tmpBuffer;
    int maxVertices;
    int maxIndices;
    IntBuffer vertices;
    ShortBuffer indices;

public:
    GLVertices3D(GLGraphics* glGraphics, int maxVertices, int maxIndices, bool hasColor, bool hasTexCoords, bool hasNormals);
    void setVertices(float* vertices, int offset, int length);
    void setIndices(short* indices, int offset, int length);
    void bind();
    void draw(int primitiveType, int offset, int numVertices);
    void unbind();
    int getNumIndices();
    int getNumVertices();
};

#endif // GLVERTICES3D_H
