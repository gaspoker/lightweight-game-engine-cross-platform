class GLColor {
public:
    static const GLColor BLACK;
    static const GLColor WHITE;
    static const GLColor RED;
    static const GLColor GREEN;
    static const GLColor BLUE;
    static const GLColor YELLOW;
    static const GLColor CYAN;
    static const GLColor PURPLE;

    float r;
    float g;
    float b;
    float a;

    GLColor(float r, float g, float b, float a) : r(r), g(g), b(b), a(a) {}

    GLColor(float n) : r(n), g(n), b(n), a(n) {}

    GLColor(int color) {
        r = (color >> 16) & 0xFF;
        g = (color >> 8) & 0xFF;
        b = color & 0xFF;
        a = color >>> 24;
    }

    GLColor(int r, int g, int b, int a) {
        this->r = (float)r / 255;
        this->g = (float)g / 255;
        this->b = (float)b / 255;
        this->a = (float)a / 255;
    }

    GLColor(const GLColor& color) : r(color.r), g(color.g), b(color.b), a(color.a) {}

    void setValues(const GLColor& src) {
        r = src.r;
        g = src.g;
        b = src.b;
        a = src.a;
    }

    void setValues(int color) {
        r = (color >> 16) & 0xFF;
        g = (color >> 8) & 0xFF;
        b = color & 0xFF;
        a = color >>> 24;
    }

    void setValues(float r, float g, float b, float a) {
        this->r = r;
        this->g = g;
        this->b = b;
        this->a = a;
    }

    void setValues(int r, int g, int b, int a) {
        this->r = (float)r / 255;
        this->g = (float)g / 255;
        this->b = (float)b / 255;
        this->a = (float)a / 255;
    }

    void multiply(float n) {
        r *= n;
        g *= n;
        b *= n;
        a *= n;
    }

    void multiply(const GLColor& color) {
        r *= color.r;
        g *= color.g;
        b *= color.b;
        a *= color.a;
    }

    int toInt() {
        return (static_cast<int>(a * 255) << 24) |
               (static_cast<int>(r * 255) << 16) |
               (static_cast<int>(g * 255) << 8) |
               static_cast<int>(b * 255);
    }

    bool equals(const GLColor& color) {
        return r == color.r && g == color.g && b == color.b && a == color.a;
    }

    bool equals(float r, float g, float b, float a) {
        return r == this->r && g == this->g && b == this->b && a == this->a;
    }

    string toString() {
        stringstream ss;
        ss << "#" << hex << setfill('0') << setw(2) << static_cast<int>(r * 255)
           << setw(2) << static_cast<int>(g * 255)
           << setw(2) << static_cast<int>(b * 255)
           << setw(2) << static_cast<int>(a * 255);
        return ss.str();
    }

    static int getInterpolatedValue(int color1, int color2, float interValue) {
        int a1 = color1 >>> 24;
        int r1 = (color1 >> 16) & 0xFF;
        int g1 = (color1 >> 8) & 0xFF;
        int b1 = color1 & 0xFF;

        int a2 = color2 >>> 24;
        int r2 = (color2 >> 16) & 0xFF;
        int g2 = (color2 >> 8) & 0xFF;
        int b2 = color2 & 0xFF;

        return (static_cast<int>(a1 + (a2 - a1) * interValue) << 24) |
               (static_cast<int>(r1 + (r2 - r1) * interValue) << 16) |
               (static_cast<int>(g1 + (g2 - g1) * interValue) << 8) |
               static_cast<int>(b1 + (b2 - b1) * interValue);
    }

    static GLColor createInterpolatedColor(int color1, int color2, float interValue) {
        return GLColor(getInterpolatedValue(color1, color2, interValue));
    }

    static GLColor createInterpolatedColor(const GLColor& color1, const GLColor& color2, float interValue) {
        return createInterpolatedColor(color1.toInt(), color2.toInt(), interValue);
    }
};

const GLColor GLColor::BLACK(0, 0, 0, 1);
const GLColor GLColor::WHITE(1, 1, 1, 1);
const GLColor GLColor::RED(1, 0, 0, 1);
const GLColor GLColor::GREEN(0, 1, 0, 1);
const GLColor GLColor::BLUE(0, 0, 1, 1);
const GLColor GLColor::YELLOW(1, 1, 0, 1);
const GLColor GLColor::CYAN(0, 1, 1, 1);
const GLColor GLColor::PURPLE(1, 0, 1, 1);
