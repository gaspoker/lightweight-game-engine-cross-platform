#include <GLES/gl.h>

class GLGame : public Renderer {
private:
    enum GLGameState {
        Initialized,
        Running,
        Paused,
        Finished,
        Idle
    };

    GLSurfaceView* glView;
    GLGraphics graphics;
    IAudio* audio;
    IInput* input;
    IFileIO* fileIO;
    Scene* scene;
    GLGameState state = GLGameState::Initialized;
    std::mutex stateChanged;
    long startTime = 0;
    float width = 0;
    float height = 0;

public:
    GLGame() : glView(nullptr), graphics(nullptr), audio(nullptr), input(nullptr), fileIO(nullptr), scene(nullptr), stateChanged() {}

    void onCreate(android_app* app) override {
        glView = new GLSurfaceView(app->activity);
        glView->setRenderer(this);
        app->activity->setContentView(glView);
        graphics = new GLGraphics(glView);
        fileIO = new FileIO(app->activity);
        audio = new Audio(app->activity);
        input = new Input(app->activity, glView, 1, 1);

        //loadAssets();
    }

    void onResume() override {
        glView->onResume();
    }

    void onPause() override {
        stateChanged.lock();
        if (isFinishing())
            state = GLGameState::Finished;
        else
            state = GLGameState::Paused;
        stateChanged.unlock();
        glView->onPause();
    }

    void onSurfaceCreated(GL10 gl, EGLConfig config) override {
        graphics->setGL(gl);
        loadAssets();

        stateChanged.lock();
        if (state == GLGameState::Initialized) {
            width = graphics->getWidth();
            height = graphics->getHeight();
            scene = getMainScene();
        }
        state = GLGameState::Running;
        scene->resume();
        startTime = System.nanoTime();
        stateChanged.unlock();
    }

    void onSurfaceChanged(GL10 gl, int width, int height) override {}

    void onDrawFrame(GL10 gl) override {
        GLGameState state = GLGameState::Idle;

        stateChanged.lock();
        state = this->state;
        stateChanged.unlock();

        if (state == GLGameState::Running) {
            float deltaTime = (System.nanoTime() - startTime) / 1000000000.0f; // in seconds
            startTime = System.nanoTime();
            scene->update(deltaTime);
            scene->collider->update(startTime, deltaTime);
            scene->present(deltaTime);
            scene->collider->present(deltaTime);
        }

        if (state == GLGameState::Paused) {
            scene->pause();
            stateChanged.lock();
            this->state = GLGameState::Idle;
            stateChanged.unlock();
        }

        if (state == GLGameState::Finished) {
            scene->pause();
            scene->dispose();
            stateChanged.lock();
            this->state = GLGameState::Idle;
            stateChanged.unlock();
        }
    }

    GLGraphics getGLGraphics() {
        return graphics;
    }

    GLSurfaceView getGLView() {
        return glView;
    }

    IInput getInput() {
        return input;
    }

    IFileIO getFileIO() {
        return fileIO;
    }

    IGraphics getGraphics() {
        throw std::runtime_error("We are using OpenGL!");
    }

    IAudio getAudio() {
        return audio;
    }

    void setScene(Scene newScene) {
        if (newScene == nullptr)
            throw std::invalid_argument("Scene must not be null");

        scene->pause();
        scene->dispose();
        newScene->resume();
        newScene->update(0);
        scene = newScene;
    }

    Scene getScene() {
        return scene;
    }

    // Full Screen Immersive Mode
    void onWindowFocusChanged(bool hasFocus) override {
        if (hasFocus) {
            hideSystemUI();
        }
    }

    void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        ANativeActivity_setWindowFlags(app->activity, AWINDOW_FLAG_FULLSCREEN | AWINDOW_FLAG_LAYOUT_IN_SCREEN, 0);
    }

    void showSystemUI() {
        ANativeActivity_setWindowFlags(app->activity, 0, AWINDOW_FLAG_FULLSCREEN | AWINDOW_FLAG_LAYOUT_IN_SCREEN);
    }

};
