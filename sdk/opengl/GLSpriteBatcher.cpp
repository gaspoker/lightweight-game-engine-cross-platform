#include "GLGraphics.h"
#include "GLTexture.h"
#include "GLTextureRegion.h"
#include "GLColor.h"

class GLSpriteBatcher {
private:
    float* verticesBuffer;
    int bufferIndex;
    GLVertices* vertices;
    int numSprites;

public:
    GLSpriteBatcher(GLGraphics* glGraphics, int maxSprites) {
        verticesBuffer = new float[maxSprites * 8 * 4]; // 8 componentes, 4 vertices
        vertices = new GLVertices(glGraphics, maxSprites * 4, maxSprites * 6, true, true);
        bufferIndex = 0;
        numSprites = 0;

        short* indices = new short[maxSprites * 6];
        int len = maxSprites * 6;
        short j = 0;
        for (int i = 0; i < len; i += 6, j += 4) {
            indices[i + 0] = (short)(j + 0);
            indices[i + 1] = (short)(j + 1);
            indices[i + 2] = (short)(j + 2);
            indices[i + 3] = (short)(j + 2);
            indices[i + 4] = (short)(j + 3);
            indices[i + 5] = (short)(j + 0);
        }
        vertices->setIndices(indices, 0, len);
        delete[] indices;
    }

    ~GLSpriteBatcher() {
        delete[] verticesBuffer;
        delete vertices;
    }

    void beginBatch(GLTexture* texture) {
        texture->bind();
        numSprites = 0;
        bufferIndex = 0;
    }

    void endBatch() {
        vertices->setVertices(verticesBuffer, 0, bufferIndex);
        vertices->bind();
        vertices->draw(GL_TRIANGLES, 0, numSprites * 6);
        vertices->unbind();
    }

    void drawSpriteTopLeft(float x, float y, float width, float height, GLTextureRegion* region, int color, float alpha) {
        float x1 = x;
        float y1 = y - height;
        float x2 = x + width;
        float y2 = y;
        GLColor* glColor = new GLColor(color);

        verticesBuffer[bufferIndex++] = x1; // Vertice coordinate
        verticesBuffer[bufferIndex++] = y1; // Vertice coordinate
        verticesBuffer[bufferIndex++] = glColor->r; // R color component
        verticesBuffer[bufferIndex++] = glColor->g; // G color component
        verticesBuffer[bufferIndex++] = glColor->b; // B color component
        verticesBuffer[bufferIndex++] = alpha; // Alpha channel
        verticesBuffer[bufferIndex++] = region->u1; // Texture coordinate
        verticesBuffer[bufferIndex++] = region->v2; // Texture coordinate

        verticesBuffer[bufferIndex++] = x2;
        verticesBuffer[bufferIndex++] = y1;
        verticesBuffer[bufferIndex++] = glColor->r; // R color component
        verticesBuffer[bufferIndex++] = glColor->g; // G color component
        verticesBuffer[bufferIndex++] = glColor->b; // B color component
        verticesBuffer[bufferIndex++] = alpha; // Alpha channel
        verticesBuffer[bufferIndex++] = region->u2;
        verticesBuffer[bufferIndex++] = region->v2;

        verticesBuffer[bufferIndex++] = x2;
        verticesBuffer[bufferIndex++] = y2;
        verticesBuffer[bufferIndex++] = glColor->r; // R color component
        verticesBuffer[bufferIndex++] = glColor->g; // G color component
        verticesBuffer[bufferIndex++] = glColor->b; // B color component
        verticesBuffer[bufferIndex++] = alpha; // Alpha channel
        verticesBuffer[bufferIndex++] = region->u2;
        verticesBuffer[bufferIndex++] = region->v1;

        verticesBuffer[bufferIndex++] = x1;
        verticesBuffer[bufferIndex++] = y2;
        verticesBuffer[bufferIndex++] = glColor->r; // R color component
        verticesBuffer[bufferIndex++] = glColor->g; // G color component
        verticesBuffer[bufferIndex++] = glColor->b; // B color component
        verticesBuffer[bufferIndex++] = alpha; // Alpha channel
        verticesBuffer[bufferIndex++] = region->u1;
        verticesBuffer[bufferIndex++] = region->v1;

        numSprites++;
        delete glColor;
    }

    void drawSprite(float x, float y, float width, float height, GLTextureRegion* region, int color, float alpha) {
        float halfWidth = width / 2;
        float halfHeight = height / 2;
        float x1 = x - halfWidth;
        float y1 = y - halfHeight;
        float x2 = x + halfWidth;
        float y2 = y + halfHeight;
        GLColor* glColor = new GLColor(color);

        verticesBuffer[bufferIndex++] = x1;
        verticesBuffer[bufferIndex++] = y1;
        verticesBuffer[bufferIndex++] = glColor->r; // R color component
        verticesBuffer[bufferIndex++] = glColor->g; // G color component
        verticesBuffer[bufferIndex++] = glColor->b; // B color component
        verticesBuffer[bufferIndex++] = alpha; // Alpha channel
        verticesBuffer[bufferIndex++] = region->u1;
        verticesBuffer[bufferIndex++] = region->v2;

        verticesBuffer[bufferIndex++] = x2;
        verticesBuffer[bufferIndex++] = y1;
        verticesBuffer[bufferIndex++] = glColor->r; // R color component
        verticesBuffer[bufferIndex++] = glColor->g; // G color component
        verticesBuffer[bufferIndex++] = glColor->b; // B color component
        verticesBuffer[bufferIndex++] = alpha; // Alpha channel
        verticesBuffer[bufferIndex++] = region->u2;
        verticesBuffer[bufferIndex++] = region->v2;

        verticesBuffer[bufferIndex++] = x2;
        verticesBuffer[bufferIndex++] = y2;
        verticesBuffer[bufferIndex++] = glColor->r; // R color component
        verticesBuffer[bufferIndex++] = glColor->g; // G color component
        verticesBuffer[bufferIndex++] = glColor->b; // B color component
        verticesBuffer[bufferIndex++] = alpha; // Alpha channel
        verticesBuffer[bufferIndex++] = region->u2;
        verticesBuffer[bufferIndex++] = region->v1;

        verticesBuffer[bufferIndex++] = x1;
        verticesBuffer[bufferIndex++] = y2;
        verticesBuffer[bufferIndex++] = glColor->r; // R color component
        verticesBuffer[bufferIndex++] = glColor->g; // G color component
        verticesBuffer[bufferIndex++] = glColor->b; // B color component
        verticesBuffer[bufferIndex++] = alpha; // Alpha channel
        verticesBuffer[bufferIndex++] = region->u1;
        verticesBuffer[bufferIndex++] = region->v1;

        numSprites++;
        delete glColor;
    }

    void drawSprite(float x, float y, float width, float height, float angle, GLTextureRegion* region, int color, float alpha) {
        float halfWidth = width / 2;
        float halfHeight = height / 2;
        float cos = std::cos(angle);
        float sin = std::sin(angle);

        float x1 = -halfWidth * cos - (-halfHeight) * sin;
        float y1 = -halfWidth * sin + (-halfHeight) * cos;
        float x2 = halfWidth * cos - (-halfHeight) * sin;
        float y2 = halfWidth * sin + (-halfHeight) * cos;
        float x3 = halfWidth * cos - halfHeight * sin;
        float y3 = halfWidth * sin + halfHeight * cos;
        float x4 = -halfWidth * cos - halfHeight * sin;
        float y4 = -halfWidth * sin + halfHeight * cos;
        GLColor* glColor = new GLColor(color);

        x1 += x;
        y1 += y;
        x2 += x;
        y2 += y;
        x3 += x;
        y3 += y;
        x4 += x;
        y4 += y;

        verticesBuffer[bufferIndex++] = x1;
        verticesBuffer[bufferIndex++] = y1;
        verticesBuffer[bufferIndex++] = glColor->r; // R color component
        verticesBuffer[bufferIndex++] = glColor->g; // G color component
        verticesBuffer[bufferIndex++] = glColor->b; // B color component
        verticesBuffer[bufferIndex++] = alpha; // Alpha channel
        verticesBuffer[bufferIndex++] = region->u1;
        verticesBuffer[bufferIndex++] = region->v2;

        verticesBuffer[bufferIndex++] = x2;
        verticesBuffer[bufferIndex++] = y2;
        verticesBuffer[bufferIndex++] = glColor->r; // R color component
        verticesBuffer[bufferIndex++] = glColor->g; // G color component
        verticesBuffer[bufferIndex++] = glColor->b; // B color component
        verticesBuffer[bufferIndex++] = alpha; // Alpha channel
        verticesBuffer[bufferIndex++] = region->u2;
        verticesBuffer[bufferIndex++] = region->v2;

        verticesBuffer[bufferIndex++] = x3;
        verticesBuffer[bufferIndex++] = y3;
        verticesBuffer[bufferIndex++] = glColor->r; // R color component
        verticesBuffer[bufferIndex++] = glColor->g; // G color component
        verticesBuffer[bufferIndex++] = glColor->b; // B color component
        verticesBuffer[bufferIndex++] = alpha; // Alpha channel
        verticesBuffer[bufferIndex++] = region->u2;
        verticesBuffer[bufferIndex++] = region->v1;

        verticesBuffer[bufferIndex++] = x4;
        verticesBuffer[bufferIndex++] = y4;
        verticesBuffer[bufferIndex++] = glColor->r; // R color component
        verticesBuffer[bufferIndex++] = glColor->g; // G color component
        verticesBuffer[bufferIndex++] = glColor->b; // B color component
        verticesBuffer[bufferIndex++] = alpha; // Alpha channel
        verticesBuffer[bufferIndex++] = region->u1;
        verticesBuffer[bufferIndex++] = region->v1;

        numSprites++;
        delete glColor;
    }
};
