#ifndef GLTEXTURE_H
#define GLTEXTURE_H

#include "GLGraphics.h"
#include "IFileIO.h"

class GLTexture {
private:
    GLGraphics* glGraphics;
    IFileIO* fileIO;
    std::string fileName;
    GLuint textureId;
    int minFilter;
    int magFilter;
    int width;
    int height;
    bool mipmapped;

    void load();
    void createMipmaps(Bitmap* bitmap);
    
public:
    GLTexture(GLGame* glGame, std::string fileName);
    GLTexture(GLGame* glGame, std::string fileName, bool mipmapped);
    void reload();
    void setFilters(int minFilter, int magFilter);
    void bind();
    void dispose();
};

#endif // GLTEXTURE_H
