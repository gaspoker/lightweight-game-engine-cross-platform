#include "GLTextureRegion.h"

class GLAnimation {
private:
    GLTextureRegion** keyFrames;
    int numKeyFrames;
    float frameDuration;
    bool repeat;
    bool hideOnComplete;
    bool onComplete;
    float width;
    float height;

public:
    GLAnimation(float frameDuration, bool repeat, bool hideOnComplete, GLTextureRegion** keyFrames, int numKeyFrames)
        : frameDuration(frameDuration), repeat(repeat), hideOnComplete(hideOnComplete), keyFrames(keyFrames), numKeyFrames(numKeyFrames), onComplete(false) {
        this->width = keyFrames[0]->getWidth();
        this->height = keyFrames[0]->getHeight();
    }

    GLTextureRegion* getKeyFrame(float stateTime) {
        int frameNumber = (int)(stateTime / frameDuration);

        if (frameNumber > numKeyFrames - 1 && hideOnComplete) {
            if (!onComplete) {
                onComplete = true;
                // Trigger onComplete event
            }
            return nullptr;
        }

        if (frameNumber <= numKeyFrames - 1)
            onComplete = false;

        if (!repeat) {
            frameNumber = std::min(numKeyFrames - 1, frameNumber);
        } else {
            frameNumber = frameNumber % numKeyFrames;
        }

        return keyFrames[frameNumber];
    }

    void setAnimationListener(/* AnimationListener listener */) {
    }

    int getFramesLength() {
        return numKeyFrames;
    }
};
