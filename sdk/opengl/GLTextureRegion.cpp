#include "GLTexture.h"

class GLTextureRegion {
public:
    const float width, height;
    const float u1, v1;
    const float u2, v2;
    const GLTexture* texture;

    GLTextureRegion(const GLTexture* texture, float x, float y, float width, float height) :
        u1(x / texture->width), v1(y / texture->height),
        u2(u1 + width / texture->width), v2(v1 + height / texture->height),
        texture(texture), width(width), height(height) {}
};
