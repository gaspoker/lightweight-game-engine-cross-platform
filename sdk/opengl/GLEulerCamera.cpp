#include <GLES/gl.h>
#include <android/opengl.h>
#include <android/matrix.h>

#include "geometry/Vector3D.h"

class GLEulerCamera {
private:
    Vector3D position;
    float yaw;
    float pitch;
    float fieldOfView;
    float aspectRatio;
    float near;
    float far;

public:
    GLEulerCamera(float fieldOfView, float aspectRatio, float near, float far) : 
        fieldOfView(fieldOfView), aspectRatio(aspectRatio), near(near), far(far) {}

    Vector3D getPosition() {
        return position;
    }

    float getYaw() {
        return yaw;
    }

    float getPitch() {
        return pitch;
    }

    void setAngles(float yaw, float pitch) {
        if (pitch < -90)
            pitch = -90;
        if (pitch > 90)
            pitch = 90;
        this->yaw = yaw;
        this->pitch = pitch;
    }

    void rotate(float yawInc, float pitchInc) {
        this->yaw += yawInc;
        this->pitch += pitchInc;
        if (pitch < -90)
            pitch = -90;
        if (pitch > 90)
            pitch = 90;
    }

    void setMatrices() {
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        gluPerspective(fieldOfView, aspectRatio, near, far);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glRotatef(-pitch, 1, 0, 0);
        glRotatef(-yaw, 0, 1, 0);
        glTranslatef(-position.x, -position.y, -position.z);
    }

    Vector3D getDirection() {
        float matrix[16];
        float inVec[4] = {0, 0, -1, 1};
        float outVec[4];
        Vector3D direction;

        memset(matrix, 0, sizeof(matrix));
        glLoadIdentity();
        glRotatef(yaw, 0, 1, 0);
        glRotatef(pitch, 1, 0, 0);
        glGetFloatv(GL_MODELVIEW_MATRIX, matrix);
        glLoadIdentity();

        __android_log_print(ANDROID_LOG_DEBUG, "Direction Matrix", "Matrix: %f %f %f %f", matrix[0], matrix[1], matrix[2], matrix[3]);

        glTranslatef(position.x, position.y, position.z);
        glMultMatrixf(matrix);
        glTranslatef(0, 0, -1);

        glGetFloatv(GL_MODELVIEW_MATRIX, matrix);

        __android_log_print(ANDROID_LOG_DEBUG, "Direction Matrix", "Matrix: %f %f %f %f", matrix[0], matrix[1], matrix[2], matrix[3]);

        glLoadIdentity();

        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glLoadIdentity();
        glTranslatef(0, 0, -1);
        glMultMatrixf(matrix);
        glGetFloatv(GL_MODELVIEW_MATRIX, matrix);
        glPopMatrix();

        direction.set(matrix[0], matrix[1], matrix[2]);
        return direction;
    }
};
