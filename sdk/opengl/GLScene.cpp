#include "GLGraphics.h"
#include "GLGame.h"

class GLScene : public Scene {
protected:
    GLGraphics* glGraphics;
    GLGame* glGame;

public:
    GLScene(IGame* game) : Scene(game) {
        glGame = dynamic_cast<GLGame*>(game);
        glGraphics = glGame->getGLGraphics();
    }

    virtual ~GLScene() {}
};
