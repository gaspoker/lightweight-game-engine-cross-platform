#include "GLGraphics.h"
#include "IFileIO.h"
#include <GLES/gl.h>
#include <GLES/glext.h>
#include <android/bitmap.h>
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>

class GLTexture {
private:
    GLGraphics* glGraphics;
    IFileIO* fileIO;
    std::string fileName;
    GLuint textureId;
    GLint minFilter;
    GLint magFilter;
    int width;
    int height;
    bool mipmapped;

public:
    GLTexture(GLGame* glGame, const std::string& fileName) {
        this->glGraphics = glGame->getGLGraphics();
        this->fileIO = glGame->getFileIO();
        this->fileName = fileName;
        this->mipmapped = false;
        load();
    }

    GLTexture(GLGame* glGame, const std::string& fileName, bool mipmapped) {
        this->glGraphics = glGame->getGLGraphics();
        this->fileIO = glGame->getFileIO();
        this->fileName = fileName;
        this->mipmapped = mipmapped;
        load();
    }

    ~GLTexture() {
        dispose();
    }

    void load() {
        GL10* gl = glGraphics->getGL();
        GLuint textureIds[1];
        glGenTextures(1, textureIds);
        textureId = textureIds[0];

        AAsset* asset = nullptr;
        try {
            asset = fileIO->readAsset(fileName.c_str());
            if (asset == nullptr) {
                throw std::runtime_error("Failed to load asset: " + fileName);
            }

            AAssetManager* assetManager = fileIO->getAssetManager();
            AAsset* asset = AAssetManager_open(assetManager, fileName.c_str(), AASSET_MODE_UNKNOWN);
            if (asset == nullptr) {
                throw std::runtime_error("Failed to open asset: " + fileName);
            }

            off_t assetLength = AAsset_getLength(asset);
            std::vector<char> buffer(assetLength);
            AAsset_read(asset, buffer.data(), assetLength);
            AAsset_close(asset);

            // Load bitmap from memory buffer
            AndroidBitmapInfo bitmapInfo;
            void* bitmapPixels;
            AndroidBitmap_getInfoFromMemory((void*)buffer.data(), assetLength, &bitmapInfo);
            AndroidBitmap_lockPixels((void*)buffer.data(), &bitmapPixels);

            // Create OpenGL texture
            glBindTexture(GL_TEXTURE_2D, textureId);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, bitmapInfo.width, bitmapInfo.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, bitmapPixels);

            glBindTexture(GL_TEXTURE_2D, 0);
            width = bitmapInfo.width;
            height = bitmapInfo.height;

            AndroidBitmap_unlockPixels((void*)buffer.data());
        } catch (const std::exception& e) {
            throw std::runtime_error("Couldn't load texture '" + fileName + "': " + e.what());
        } finally {
            if (asset != nullptr) {
                AAsset_close(asset);
            }
        }
    }

    void reload() {
        load();
        bind();
        setFilters(minFilter, magFilter);
        glGraphics->getGL()->glBindTexture(GL_TEXTURE_2D, 0);
    }

    void setFilters(GLint minFilter, GLint magFilter) {
        this->minFilter = minFilter;
        this->magFilter = magFilter;
        GL10* gl = glGraphics->getGL();
        gl->glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minFilter);
        gl->glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magFilter);
        gl->glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        gl->glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    }

    void bind() {
        GL10* gl = glGraphics->getGL();
        gl->glBindTexture(GL_TEXTURE_2D, textureId);
    }

    void dispose() {
        GL10* gl = glGraphics->getGL();
        gl->glBindTexture(GL_TEXTURE_2D, textureId);
        GLuint textureIds[1] = { textureId };
        gl->glDeleteTextures(1, textureIds);
    }
};
