#include <GLES/gl.h>
#include <android/native_window.h>
#include <android/native_window_jni.h>
#include <jni.h>
#include <vector>

class GLGraphics : public IGraphics {
private:
    GLSurfaceView* glView;
    GL10 gl;

public:
    GLGraphics(GLSurfaceView* glView) : glView(glView) {}

    GL10 getGL() {
        return gl;
    }

    void setGL(GL10 gl) {
        this->gl = gl;
    }

    IPixmap newPixmap(std::string fileName, PixmapFormat format) override {
        return nullptr;
    }

    void clear(int color) override {
        setColor(color);
        glClear(GL_COLOR_BUFFER_BIT);
    }

    void drawPixel(float x, float y, int color, float alpha) override {
        GLfloat vertices[] = {x, y};
        glVertexPointer(2, GL_FLOAT, 0, vertices);
        glEnableClientState(GL_VERTEX_ARRAY);

        setColor(color, alpha);
        glDrawArrays(GL_POINTS, 0, 1);

        glDisableClientState(GL_VERTEX_ARRAY);
    }

    void drawPoint(float x, float y, int color) {
        GLfloat vertices[] = {x, y};
        glVertexPointer(2, GL_FLOAT, 0, vertices);
        glEnableClientState(GL_VERTEX_ARRAY);

        setColor(color);
        glPointSize(2);
        glDrawArrays(GL_POINTS, 0, 1);

        glDisableClientState(GL_VERTEX_ARRAY);
    }

    void drawPoint(Vector point, int color) {
        drawPoint(point.x, point.y, color);
    }

    void drawPoints(Vector points[], int numberOfPoints) {
        std::vector<GLfloat> vertices;
        for (int i = 0; i < numberOfPoints; i++) {
            vertices.push_back(points[i].x);
            vertices.push_back(points[i].y);
        }

        glVertexPointer(2, GL_FLOAT, 0, vertices.data());
        glEnableClientState(GL_VERTEX_ARRAY);

        glDrawArrays(GL_POINTS, 0, numberOfPoints);

        glDisableClientState(GL_VERTEX_ARRAY);
    }

    void drawLine(float x, float y, float x2, float y2, int color, float alpha) {
        GLfloat vertices[] = {x, y, x2, y2};
        glVertexPointer(2, GL_FLOAT, 0, vertices);
        glEnableClientState(GL_VERTEX_ARRAY);

        setColor(color, alpha);
        glDrawArrays(GL_LINES, 0, 2);

        glDisableClientState(GL_VERTEX_ARRAY);
    }

    void drawRect(float x, float y, float width, float height, int color, float alpha) {
        std::vector<Vector> poli;
        poli.push_back(Vector(x, y));
        poli.push_back(Vector(x + width, y));
        poli.push_back(Vector(x + width, y + height));
        poli.push_back(Vector(x, y + height));

        setColor(color, alpha);
        drawPoly(poli, true, color, alpha);
    }

    void drawPoly(std::vector<Vector> poli, bool closePolygon, int color, float alpha) {
        int numberOfPoints = poli.size();
        std::vector<GLfloat> vertices;
        for (int i = 0; i < numberOfPoints; i++) {
            vertices.push_back(poli[i].x);
            vertices.push_back(poli[i].y);
        }

        glVertexPointer(2, GL_FLOAT, 0, vertices.data());
        glEnableClientState(GL_VERTEX_ARRAY);

        setColor(color, alpha);
        if (closePolygon)
            glDrawArrays(GL_LINE_LOOP, 0, numberOfPoints);
        else
            glDrawArrays(GL_LINE_STRIP, 0, numberOfPoints);

        glDisableClientState(GL_VERTEX_ARRAY);
    }

    void drawCircle(float centerX, float centerY, float r, float a, int segments, bool drawLineToCenter, int color, float alpha) {
        std::vector<GLfloat> vertices;
        float coef = 2.0f * M_PI / segments;

        for (int i = 0; i <= segments; i++) {
            float rads = i * coef;
            float j = r * cos(rads + a) + centerX;
            float k = r * sin(rads + a) + centerY;

            vertices.push_back(j);
            vertices.push_back(k);
        }

        if (drawLineToCenter) {
            vertices.push_back(centerX);
            vertices.push_back(centerY);
        }

        glVertexPointer(2, GL_FLOAT, 0, vertices.data());
        glEnableClientState(GL_VERTEX_ARRAY);

        setColor(color, alpha);
        glDrawArrays(GL_LINE_STRIP, 0, segments + (drawLineToCenter ? 2 : 1));

        glDisableClientState(GL_VERTEX_ARRAY);
    }

    void drawQuadBezier(GLfloat originX, GLfloat originY, GLfloat controlX, GLfloat controlY, GLfloat destinationX, GLfloat destinationY, int segments) {
        std::vector<GLfloat> vertices;
        GLfloat t = 0.0f;
        for (int i = 0; i < segments; i++) {
            GLfloat x = pow(1 - t, 2) * originX + 2.0f * (1 - t) * t * controlX + t * t * destinationX;
            GLfloat y = pow(1 - t, 2) * originY + 2.0f * (1 - t) * t * controlY + t * t * destinationY;
            vertices.push_back(x);
            vertices.push_back(y);
            t += 1.0f / segments;
        }
        vertices.push_back(destinationX);
        vertices.push_back(destinationY);

        glVertexPointer(2, GL_FLOAT, 0, vertices.data());
        glEnableClientState(GL_VERTEX_ARRAY);

        glDrawArrays(GL_LINE_STRIP, 0, segments + 1);

        glDisableClientState(GL_VERTEX_ARRAY);
    }

    void drawCubicBezier(GLfloat originX, GLfloat originY, GLfloat control1X, GLfloat control1Y, GLfloat control2X, GLfloat control2Y, GLfloat destinationX, GLfloat destinationY, int segments) {
        std::vector<GLfloat> vertices;
        GLfloat t = 0;
        for (int i = 0; i < segments; i++) {
            GLfloat x = pow(1 - t, 3) * originX + 3.0f * pow(1 - t, 2) * t * control1X + 3.0f * (1 - t) * pow(t, 2) * control2X + pow(t, 3) * destinationX;
            GLfloat y = pow(1 - t, 3) * originY + 3.0f * pow(1 - t, 2) * t * control1Y + 3.0f * (1 - t) * pow(t, 2) * control2Y + pow(t, 3) * destinationY;
            vertices.push_back(x);
            vertices.push_back(y);
            t += 1.0f / segments;
        }
        vertices.push_back(destinationX);
        vertices.push_back(destinationY);

        glVertexPointer(2, GL_FLOAT, 0, vertices.data());
        glEnableClientState(GL_VERTEX_ARRAY);

        glDrawArrays(GL_LINE_STRIP, 0, segments + 1);

        glDisableClientState(GL_VERTEX_ARRAY);
    }

    void drawPixmap(IPixmap pixmap, int x, int y, int srcX, int srcY, int srcWidth, int srcHeight) override {}

    void drawPixmap(IPixmap pixmap, int x, int y) override {}

    int getWidth() {
        return glView->getWidth();
    }

    int getHeight() {
        return glView->getHeight();
    }

    void setColor(int color) {
        GLColor glColor(color);
        glColor4f(glColor.r, glColor.g, glColor.b, glColor.a);
    }

    void setColor(int color, float alpha) {
        GLColor glColor(color);
        glColor4f(glColor.r, glColor.g, glColor.b, alpha);
    }

    void enableTexture2D() {
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_TEXTURE_2D);
    }

    void disableTexture2D() {
        glDisable(GL_TEXTURE_2D);
        glDisable(GL_BLEND);
    }
};
