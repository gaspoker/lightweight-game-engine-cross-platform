#include <GLES/gl.h>
#include <vector>

class GLVertices {
private:
    const int BYTES_PER_FLOAT = 4;

    GLGraphics* glGraphics;
    bool hasColor;
    bool hasTexCoords;
    int vertexSize;
    std::vector<int> vertices;
    std::vector<int> tmpBuffer;
    std::vector<short> indices;

public:
    GLVertices(GLGraphics* glGraphics, int maxVertices, int maxIndices, bool hasColor, bool hasTexCoords) :
            glGraphics(glGraphics), hasColor(hasColor), hasTexCoords(hasTexCoords) {
        vertexSize = (2 + (hasColor ? 4 : 0) + (hasTexCoords ? 2 : 0)) * BYTES_PER_FLOAT;
        vertices.reserve(maxVertices * vertexSize / 4);

        if (maxIndices > 0) {
            indices.reserve(maxIndices);
        }
    }

    void setVertices(float* vertices, int offset, int length) {
        tmpBuffer.reserve(length);
        int len = offset + length;
        for (int i = offset; i < len; i++) {
            tmpBuffer.push_back(*(int*)(&vertices[i]));
        }
        this->vertices.insert(this->vertices.end(), tmpBuffer.begin(), tmpBuffer.end());
        tmpBuffer.clear();
    }

    void setIndices(short* indices, int offset, int length) {
        this->indices.insert(this->indices.end(), indices + offset, indices + offset + length);
    }

    void bind() {
        glEnableClientState(GL_VERTEX_ARRAY);
        glVertexPointer(2, GL_FLOAT, vertexSize, &vertices[0]);

        if (hasColor) {
            glEnableClientState(GL_COLOR_ARRAY);
            glColorPointer(4, GL_FLOAT, vertexSize, &vertices[2]);
        }

        if (hasTexCoords) {
            glEnableClientState(GL_TEXTURE_COORD_ARRAY);
            int texCoordOffset = hasColor ? 6 : 2;
            glTexCoordPointer(2, GL_FLOAT, vertexSize, &vertices[texCoordOffset]);
        }
    }

    void draw(int primitiveType, int offset, int numVertices) {
        if (!indices.empty()) {
            glDrawElements(primitiveType, numVertices, GL_UNSIGNED_SHORT, &indices[offset]);
        } else {
            glDrawArrays(primitiveType, offset, numVertices);
        }
    }

    void unbind() {
        if (hasTexCoords) {
            glDisableClientState(GL_TEXTURE_COORD_ARRAY);
        }

        if (hasColor) {
            glDisableClientState(GL_COLOR_ARRAY);
        }

        glDisableClientState(GL_VERTEX_ARRAY);
    }
};
