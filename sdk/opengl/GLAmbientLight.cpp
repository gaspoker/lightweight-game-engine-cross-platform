#include <GLES/gl.h>

class GLAmbientLight {
private:
    float color[4] = {0.2f, 0.2f, 0.2f, 1.0f};

public:
    void setColor(float r, float g, float b, float a) {
        color[0] = r;
        color[1] = g;
        color[2] = b;
        color[3] = a;
    }

    void enable() {
        glLightModelfv(GL_LIGHT_MODEL_AMBIENT, color);
    }
};
