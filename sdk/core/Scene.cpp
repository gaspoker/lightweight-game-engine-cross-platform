#pragma once

#include "base/IGame.h"
#include "collision/Collider.h"

class Scene : public Collider::CollisionListener {
public:
    IGame* game;
    Collider collider;

    Scene(IGame* game)
        : game(game), collider(this, this) {}

    virtual void update(float deltaTime) = 0;

    virtual void present(float deltaTime) = 0;

    virtual void pause() = 0;

    virtual void resume() = 0;

    virtual void dispose() = 0;

    // Agrego objetos al Collider
    void add() {}
};
