#include <iostream>
#include <string>
#include <cmath>
#include <vector>

#include "geometry/Rectangle.h"
#include "geometry/Vector.h"
#include "Body.h"
#include "Scene.h"
#include "BodyCollisionListener.h"
#include "BodySimpleCollisionListener.h"
#include "Collider.h"
#include "nlohmann/json.hpp" // Implement!

using namespace std;
using json = nlohmann::json;

class GameObject {
protected:
    Scene* scene;
    Body* body;
    float width, height;
    float alpha;
    float halfWidth, halfHeight;
    string type;
    int state;
    string name;
    bool active;
    bool visible;
    bool ignoreDestroy;
    float originX, originY;
    float displayOriginX, displayOriginY;

public:
    void setCollisionListener(BodyCollisionListener* listener) {
        this->body->setCollisionListener(listener);
    }

    void setSimpleCollisionListener(BodySimpleCollisionListener* listener) {
        this->body->setSimpleCollisionListener(listener);
    }

private:
    void initProperties(Scene* scene, float width, float height) {
        this->scene = scene;
        this->width = width;
        this->height = height;
        this->halfWidth = abs(this->width / 2.0f);
        this->halfHeight = abs(this->height / 2.0f);
        this->alpha = 1.0f;
        this->state = 0;
        this->name = "";
        this->type = "";
        this->active = true;
        this->visible = true;
        this->ignoreDestroy = false;
        this->originX = 0.5f;
        this->originY = 0.5f;
        this->displayOriginX = 0.0f;
        this->displayOriginY = 0.0f;
    }

public:
    GameObject(Scene* scene, Rectangle rect) {
        GameObject(scene, rect.lowerLeft.x, rect.lowerLeft.y, rect.width, rect.height);
    }

    GameObject(Scene* scene, float x, float y, float width, float height) {
        initProperties(scene, width, height);

        this->body = new Body(x, y, width, height);
        this->body->setGameObject(this);

        float cx = this->body->centerOffset.x;
        float cy = this->body->centerOffset.y;
        this->setOrigin(cx / this->getDisplayWidth(), cy / this->getDisplayHeight());

        this->setPosition(x, y);

        this->scene->collider.addBody(this->body);
    }

    GameObject(Scene* scene, float x, float y, float width, float height, json shapes, string shapeName) {
        initProperties(scene, width, height);

        this->body = new Body(x, y, shapes, shapeName);
        this->body->setGameObject(this);

        float cx = this->body->centerOffset.x;
        float cy = this->body->centerOffset.y;
        this->setOrigin(cx / this->getDisplayWidth(), cy / this->getDisplayHeight());

        this->setPosition(x, y);

        this->scene->collider.addBody(this->body);
    }

    void setBodyPosition(float x, float y) {
    }

    void setX(float x) {
        Vector bodyPosition(x - this->halfWidth + this->displayOriginX, this->body->position.y);
        this->body->setPosition(bodyPosition);
    }

    float getX() {
        return this->body->position.x + this->halfWidth - this->displayOriginX;
    }

    void incX(float inc) {
        this->setX(this->getX() + inc);
    }

    void setY(float y) {
        Vector bodyPosition(this->body->position.x, y - this->halfHeight + this->displayOriginY);
        this->body->setPosition(bodyPosition);
    }

    float getY() {
        return this->body->position.y + this->halfHeight - this->displayOriginY;
    }

    void incY(float inc) {
        this->setY(this->getY() + inc);
    }

    void setPosition(Vector v) {
        setPosition(v.x, v.y);
    }

    void setPosition(float x, float y) {
        Vector bodyPosition(x - this->halfWidth + this->displayOriginX, y - this->halfHeight + this->displayOriginY);
        this->body->setPosition(bodyPosition);
    }

    Vector getPosition() {
        return Vector(getX(), getY());
    }

    void setTopLeftPosition(float x, float y) {
        Vector bodyPosition(x + this->halfWidth + this->displayOriginX, y + this->halfHeight + this->displayOriginY);
        this->body->setPosition(bodyPosition);
    }

    void setLeftPosition(float x) {
        Vector bodyPosition(x + this->halfWidth + this->displayOriginX, this->body->position.y);
        this->body->setPosition(bodyPosition);
    }

    void setRightPosition(float x) {
        Vector bodyPosition(x + this->halfWidth + this->displayOriginX + this->width, this->body->position.y);
        this->body->setPosition(bodyPosition);
    }

    float getWidth() {
        return this->width;
    }

    float getHeight() {
        return this->height;
    }

    void setAngle(float angle) {
        this->body->setAngle(angle);
    }

    float getAngle() {
        return this->body->angle;
    }

    string getName() {
        return this->name;
    }

    void setName(string name) {
        this->name = name;
        this->body->setLabel(name);
    }

    float getDisplayWidth() {
        return abs(this->body->scale.x * this->width);
    }

    void setDisplayWidth(float width) {
        float scaleX = width / this->width;
        float scaleY = this->body->scale.y;
        this->body->scale(scaleX, scaleY, nullptr);
    }

    float getDisplayHeight() {
        return abs(this->body->scale.y * this->height);
    }

    void setDisplayHeight(float height) {
        float scaleX = this->body->scale.x;
        float scaleY = height / this->height;
        this->body->scale(scaleX, scaleY, nullptr);
    }

    void setDisplaySize(float width, float height) {
        this->setDisplayWidth(width);
        this->setDisplayHeight(height);
    }

    float getTop() {
        return this->body->position.y - this->displayOriginY;
    }

    float getLeft() {
        return this->body->position.x - this->displayOriginX;
    }

    float getBottom() {
        return this->body->position.y - this->displayOriginY + this->height;
    }

    float getRight() {
        return this->body->position.x - this->displayOriginX + this->width;
    }

    void setStatic(bool value) {
        this->body->setStatic(value);
    }

    bool isStatic() {
        return this->body->isStatic;
    }

    void setSensor(bool value) {
        this->body->isSensor = value;
    }

    bool isSensor() {
        return this->body->isSensor;
    }

    void setOrigin(float x, float y) {
        this->originX = x;
        this->originY = y;
        this->updateDisplayOrigin();
    }

    float getOriginX() {
        return this->originX;
    }

    float getOriginY() {
        return this->originY;
    }

    float getDisplayOriginX() {
        return this->displayOriginX;
    }

    void setDisplayOriginX(float value) {
        this->displayOriginX = value;
        this->originX = value / this->width;
    }

    float getDisplayOriginY() {
        return this->displayOriginY;
    }

    void setDisplayOriginY(float value) {
        this->displayOriginY = value;
        this->originY = value / this->height;
    }

    void setDisplayOrigin(float x, float y) {
        this->displayOriginX = x;
        this->displayOriginY = y;
    }

    void updateDisplayOrigin() {
        this->displayOriginX = this->originX * this->width;
        this->displayOriginY = this->originY * this->height;
    }

    void setVelocity(float velX, float velY) {
        this->body->setVelocity(Vector(velX, velY));
    }

    void setVelocityX(float vel) {
        this->body->setVelocity(Vector(vel, this->body->velocity.y));
    }

    void setVelocityY(float vel) {
        this->body->setVelocity(Vector(this->body->velocity.x, vel));
    }

    float getVelocityX() {
        return this->body->velocity.x;
    }

    float getVelocityY() {
        return this->body->velocity.y;
    }

    bool isActive() {
        return active;
    }

    void setActive(bool active) {
        this->active = active;
    }

    void setVisible(bool value) {
        visible = value;
        if (value) {
            alpha = 1.0f;
        } else {
            alpha = 0.0f;
        }
    }

    void destroy() {
        setPosition(-INFINITY, -INFINITY);
        scene->collider.removeBody(this->body);
        setActive(false);
        setVisible(false);
    }

    ~GameObject() {
        destroy();
    }

    float getAlpha() {
        return alpha;
    }

    void setAlpha(float alpha) {
        this->alpha = alpha;
    }
};
