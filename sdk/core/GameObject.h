#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include "sdk_geometry.h"
#include "sdk_core_body.h"
#include "sdk_core_collider.h"
#include "Scene.h"

class BodyCollisionListener {
};

class BodySimpleCollisionListener {
};

class GameObject {
protected:
    Scene* scene;
    Body body;
    float width, height;
    float alpha;
    float halfWidth, halfHeight;
    std::string type;
    int state;
    std::string name;
    bool active;
    bool visible;
    bool ignoreDestroy;
    float originX, originY;
    float displayOriginX, displayOriginY;

public:
    GameObject(Scene* _scene, float _width, float _height) : scene(_scene), width(_width), height(_height) {
        initProperties(_width, _height);
    }

    void setCollisionListener(BodyCollisionListener* listener) {
    }

    void setSimpleCollisionListener(BodySimpleCollisionListener* listener) {
    }

private:
    void initProperties(float _width, float _height) {
    }
};

class Scene {
public:
    Collider collider;
};
