#pragma once

#include "GameObject.h"
#include "opengl/GLAnimation.h"
#include "opengl/GLTextureRegion.h"

#include <cstdint>

class Sprite : public GameObject {
private:
    GLTextureRegion* texture;
    GLAnimation* anims;
    std::uint32_t tint;
    float sumTime;

    void initProperties(GLTextureRegion* texture, GLAnimation* anims) {
        type = "Sprite";
        this->texture = texture;
        this->anims = anims;
        tint = 0xFFFFFFFF; // Color blanco por defecto
        sumTime = 0.0f;
    }

public:
    Sprite(Scene* scene, float x, float y, float width, float height, GLAnimation* anims)
        : GameObject(scene, x, y, width, height) {
        initProperties(nullptr, anims);
    }

    Sprite(Scene* scene, float x, float y, float width, float height, GLTextureRegion* texture)
        : GameObject(scene, x, y, width, height) {
        initProperties(texture, nullptr);
    }

    Sprite(Scene* scene, float x, float y, float width, float height, GLTextureRegion* texture, JSONObject shapes, std::string shapeName)
        : GameObject(scene, x, y, width, height, shapes, shapeName) {
        initProperties(texture, nullptr);
    }

    std::uint32_t getTint() {
        return tint;
    }

    void setTint(std::uint32_t tint) {
        this->tint = tint;
    }

    void setTexture(GLTextureRegion* texture) {
        this->texture = texture;
    }

    GLTextureRegion* getTexture() {
        return texture;
    }

    GLTextureRegion* getTexture(float deltaTime) {
        sumTime += deltaTime;
        return anims->getKeyFrame(sumTime);
    }

    void resetAnims() {
        sumTime = 0.0f;
    }

    GLAnimation* getAnims() {
        return anims;
    }

    void setAnims(GLAnimation* anims) {
        if (this->anims != anims) this->anims = anims;
    }
};
