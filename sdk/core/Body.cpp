#include "Body.h"
#include "Vertices.h"
#include "Axes.h"
#include <cmath>

int Body::idGen = 0;

const float Body::INERTIA_SCALE = 4.0f;
const float Body::NEXT_COLLIDING_GROUP_ID = 1.0f;
const float Body::NEXT_NON_COLLIDING_GROUP_ID = -1.0f;
const float Body::NEXT_CATEGORY = 0x0001;

Body::Body(Vector pos, std::vector<Vertex> verts) : position(pos), vertices(verts) {
    id = ++idGen;
    type = "body";
    label = "Body";
    angle = 0.0f;
    force = Vector(0.0f, 0.0f);
    torque = 0.0f;
    positionImpulse = Vector(0.0f, 0.0f);
    previousPositionImpulse = Vector(0.0f, 0.0f);
    constraintImpulse = Vector(0.0f, 0.0f);
    totalContacts = 0;
    speed = 0.0f;
    angularSpeed = 0.0f;
    velocity = Vector(0.0f, 0.0f);
    angularVelocity = 0.0f;
    isSensor = false;
    isStatic = false;
    isSleeping = false;
    sleepCounter = 0.0f;
    motion = 0.0f;
    sleepThreshold = 60.0f;
    density = 0.001f;
    restitution = 0.0f;
    friction = 0.1f;
    frictionStatic = 0.5f;
    frictionAir = 0.01f;
    slop = 0.05f;
    timeScale = 1.0f;
    bounds = nullptr;
    circleRadius = 0.0f;
    positionPrev = nullptr;
    anglePrev = 0.0f;
    parent = nullptr;
    axes = nullptr;
    area = 0.0f;
    mass = 0.0f;
    inverseMass = 0.0f;
    inertia = 0.0f;
    inverseInertia = 0.0f;
    original = nullptr;
    gameObject = nullptr;
    region = nullptr;
}

Body::Body(float x, float y, float width, float height) {
}

Body::Body(float x, float y, JSONObject shapes, std::string shapeName) {
}

void Body::parseBody(float x, float y, JSONObject shapes, std::string shapeName) {
}

std::vector<Body> Body::parseFixture(JSONObject fixtureConfig) {
}

std::vector<Vertex> Body::parseVertices(JSONArray vertexSets) {
}

std::vector<Vertex> Body::parseVertexSet(JSONArray vertexSet) {
}

void Body::setDefaultValues(InitValues iv) {
}

void Body::initProperties() {
}

void Body::setStatic(bool isStatic) {
}

void Body::setMass(float mass) {
}

void Body::setInertia(float inertia) {
}

void Body::setSleeping(bool isSleeping) {
    bool wasSleeping = this->isSleeping;

    if (isSleeping) {
        this->isSleeping = true;
        this->sleepCounter = this->sleepThreshold;

        this->positionImpulse.x = 0.0f;
        this->positionImpulse.y = 0.0f;

        this->positionPrev.x = this->position.x;
        this->positionPrev.y = this->position.y;

        this->anglePrev = this->angle;
        this->speed = 0.0f;
        this->angularSpeed = 0.0f;
        this->motion = 0.0f;
    } else {
        this->isSleeping = false;
        this->sleepCounter = 0.0f;
    }
}

void Body::setMass(float mass) {
    float moment = this->inertia / (this->mass / 6.0f);
    this->inertia = moment * (mass / 6.0f);
    this->inverseInertia = 1.0f / this->inertia;

    this->mass = mass;
    this->inverseMass = 1.0f / this->mass;
    this->density = this->mass / this->area;
}

void Body::setDensity(float density) {
    this->setMass(density * this->area);
    this->density = density;
}

void Body::setInertia(float inertia) {
    this->inertia = inertia;
    this->inverseInertia = 1.0f / this->inertia;
}

void Body::setVertices(std::vector<Vertex> vertices) {
}

void Body::setParts(std::vector<Body> parts, bool autoHull) {
}

void Body::setCentre(Vector centre, bool relative) {
    if (!relative) {
        this->positionPrev.x = centre.x - (this->position.x - this->positionPrev.x);
        this->positionPrev.y = centre.y - (this->position.y - this->positionPrev.y);
        this->position.x = centre.x;
        this->position.y = centre.y;
    } else {
        this->positionPrev.x += centre.x;
        this->positionPrev.y += centre.y;
        this->position.x += centre.x;
        this->position.y += centre.y;
    }
}

void Body::setPosition(Vector position) {
    Vector delta = Vector::sub(position, this->position);
    this->positionPrev.x += delta.x;
    this->positionPrev.y += delta.y;
    this->position.x = position.x;
    this->position.y = position.y;

    for (Body &part : this->parts) {
        part.position.x += delta.x;
        part.position.y += delta.y;
        Vertices::translate(part.vertices, delta);
        part.bounds.update(part.vertices, this->velocity);
    }
}

void Body::setAngle(float angle) {
    float delta = angle - this->angle;
    this->anglePrev += delta;
    this->angle = angle;

    for (Body &part : this->parts) {
        part.angle += delta;
        Vertices::rotate(part.vertices, delta, this->position);
        Axes::rotate(part.axes, delta);
        part.bounds.update(part.vertices, this->velocity);
        if (&part != &this->parts[0]) {
            Vector::rotateAbout(part.position, delta, this->position, part.position);
        }
    }
}

void Body::setVelocity(Vector velocity) {
    Vector delta = Vector::sub(this->position, this->positionPrev);
    this->positionPrev.x = this->position.x - velocity.x;
    this->positionPrev.y = this->position.y - velocity.y;
    this->velocity = velocity;
    this->speed = Vector::magnitude(this->velocity);

    for (Body &part : this->parts) {
        Vertices::translate(part.vertices, delta);
        if (&part != &this->parts[0]) {
            part.position.x += delta.x;
            part.position.y += delta.y;
        }
        part.bounds.update(part.vertices, this->velocity);
    }
}

void Body::setAngularVelocity(float velocity) {
    float delta = this->angle - this->anglePrev;
    this->anglePrev = this->angle - velocity;
    this->angularVelocity = velocity;
    this->angularSpeed = std::abs(this->angularVelocity);

    for (Body &part : this->parts) {
        part.angle -= delta;
        Vertices::rotate(part.vertices, delta, this->position);
        Axes::rotate(part.axes, delta);
        part.bounds.update(part.vertices, this->velocity);
        if (&part != &this->parts[0]) {
            Vector::rotateAbout(part.position, delta, this->position, part.position);
        }
    }
}

void Body::translate(Vector translation) {
    this->setPosition(Vector::add(this->position, translation));
}

void Body::rotate(float rotation, Vector point) {
    if (point == nullptr) {
        this->setAngle(this->angle + rotation);
    } else {
        float cos = std::cos(rotation);
        float sin = std::sin(rotation);
        float dx = this->position.x - point->x;
        float dy = this->position.y - point->y;
        Vector newPoint(point->x + (dx * cos - dy * sin), point->y + (dx * sin + dy * cos));
        this->setPosition(newPoint);
        this->setAngle(this->angle + rotation);
    }
}

void Body::scale(float scaleX, float scaleY, Vector point) {
}

void Body::update(float deltaTime) {
}

void Body::applyForce(Vector position, Vector force) {
    this->force.x += force.x;
    this->force.y += force.y;
    Vector offset(position.x - this->position.x, position.y - this->position.y);
    this->torque += offset.x * force.y - offset.y * force.x;
}
