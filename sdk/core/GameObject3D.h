#include "geometry/Sphere.h"
#include "geometry/Vector3D.h"

class GameObject3D {
public:
    const Vector3D position;
    const Sphere bounds;
    const Vector3D velocity;
    const Vector3D accel;

    GameObject3D(float x, float y, float z, float radius)
        : position(x, y, z), bounds(x, y, z, radius), velocity(), accel() {}
};
