#include "GameObject.h"

class Image : public GameObject {
public:
    Image(Scene* scene, float x, float y, float width, float height)
        : GameObject(scene, x, y, width, height) {}
};
