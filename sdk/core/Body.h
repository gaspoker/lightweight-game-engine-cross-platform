#ifndef BODY_H
#define BODY_H

#include <vector>
#include <string>
#include "Vector.h"
#include "Vertex.h"
#include "Bounds.h"
#include "CollisionFilter.h"
#include "RigidBody.h"
#include "GameObject.h"
#include "Grid.h"

class Body {
public:
    static const float INERTIA_SCALE;
    static const float NEXT_COLLIDING_GROUP_ID;
    static const float NEXT_NON_COLLIDING_GROUP_ID;
    static const float NEXT_CATEGORY;

    struct InitValues {
        std::string label;
        std::vector<Vertex> vertices;
        Vector position;

        InitValues(std::string lbl, std::vector<Vertex> verts, Vector pos)
            : label(lbl), vertices(verts), position(pos) {}
    };

    struct BodyCollisionListener {
        virtual void onCollideStart(Pair data) = 0;
        virtual void onCollideActive(Pair data) = 0;
        virtual void onCollideEnd(Pair data) = 0;
    };

    struct BodySimpleCollisionListener {
        virtual void onCollide(Pair data, Body self, Body other) = 0;
    };

    BodyCollisionListener* listener;
    BodySimpleCollisionListener* simpleListener;

    static int idGen;
    int id;
    std::string type;
    std::string label;
    std::vector<Body> parts;
    float angle;
    std::vector<Vertex> vertices;
    Vector position;
    Vector force;
    float torque;
    Vector positionImpulse;
    Vector previousPositionImpulse;
    Vector constraintImpulse;
    int totalContacts;
    float speed;
    float angularSpeed;
    Vector velocity;
    float angularVelocity;
    bool isSensor;
    bool isStatic;
    bool isSleeping;
    float sleepCounter;
    float motion;
    float sleepThreshold;
    float density;
    float restitution;
    float friction;
    float frictionStatic;
    float frictionAir;
    CollisionFilter collisionFilter;
    float slop;
    float timeScale;
    Bounds bounds;
    float circleRadius;
    Vector positionPrev;
    float anglePrev;
    Body* parent;
    std::vector<Vector> axes;
    float area;
    float mass;
    float inverseMass;
    float inertia;
    float inverseInertia;
    RigidBody* original;
    GameObject* gameObject;
    Grid::Region* region;
    Vector scale;
    Vector centerOfMass;
    Vector centerOffset;
    Vector gravityScale;
    bool ignoreGravity;
    bool ignorePointer;

    Body(Vector pos, std::vector<Vertex> verts);
    Body(float x, float y, float width, float height);
    Body(float x, float y, JSONObject shapes, std::string shapeName);

    void parseBody(float x, float y, JSONObject shapes, std::string shapeName);
    std::vector<Body> parseFixture(JSONObject fixtureConfig);
    std::vector<Vertex> parseVertices(JSONArray vertexSets);
    std::vector<Vertex> parseVertexSet(JSONArray vertexSet);

    void setDefaultValues(InitValues iv);
    void initProperties();
    void setStatic(bool isStatic);

private:
    void setMass(float mass);
    void setInertia(float inertia);
};

#endif // BODY_H
