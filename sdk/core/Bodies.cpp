#pragma once

#include <vector>

#include "core/Body.h"
#include "geometry/Bounds.h"
#include "geometry/Vector.h"
#include "geometry/Vertex.h"
#include "geometry/Vertices.h"

class Bodies {
public:
    static std::vector<Body*> flagCoincidentParts(std::vector<Body*>& parts) {
        float maxDistance = 5;
        return flagCoincidentParts(parts, maxDistance);
    }

private:
    static std::vector<Body*> flagCoincidentParts(std::vector<Body*>& parts, float maxDistance) {
        for (size_t i = 0; i < parts.size(); ++i) {
            Body* partA = parts[i];

            for (size_t j = i + 1; j < parts.size(); ++j) {
                Body* partB = parts[j];

                if (Bounds::overlaps(partA->bounds, partB->bounds)) {
                    std::vector<Vertex*>& pav = partA->vertices;
                    std::vector<Vertex*>& pbv = partB->vertices;

                    // iterate vertices of both parts
                    for (size_t k = 0; k < pav.size(); ++k) {
                        for (size_t z = 0; z < pbv.size(); ++z) {
                            // find distances between the vertices
                            float da = Vector::magnitudeSquared(Vector::sub(*pav[(k + 1) % pav.size()], *pbv[z]));
                            float db = Vector::magnitudeSquared(Vector::sub(*pav[k], *pbv[(z + 1) % pbv.size()]));

                            // if both vertices are very close, consider the edge coincident (internal)
                            if (da < maxDistance && db < maxDistance) {
                                pav[k]->isInternal = true;
                                pbv[z]->isInternal = true;
                            }
                        }
                    }
                }
            }
        }
        return parts;
    }
};
