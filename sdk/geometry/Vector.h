#pragma once

#include <cmath>
#include <string>

class Vector {
public:
    static float TO_RADIANS;
    static float TO_DEGREES;
    float x, y;

    Vector();
    Vector(float x, float y);
    Vector(const Vector& other);
    Vector cpy();
    Vector set(float x, float y);
    Vector set(Vector other);
    Vector add(float x, float y);
    Vector add(Vector other);
    Vector sub(float x, float y);
    Vector sub(Vector other);
    Vector mult(float scalar);
    Vector div(float scalar);
    float len();
    Vector nor();
    Vector rotate(float angle);
    float dist(Vector other);
    float dist(float x, float y);
    float distSquared(Vector other);
    float distSquared(float x, float y);
    static Vector create(float x, float y);
    static Vector add(Vector vectorA, Vector vectorB, Vector output = nullptr);
    static Vector div(Vector vector, float scalar);
    static Vector perp(Vector vector, bool negate);
    static Vector neg(Vector vector);
    static Vector mult(Vector vector, float scalar);
    static Vector sub(Vector vectorA, Vector vectorB, Vector output = nullptr);
    static float angle(Vector vectorA, Vector vectorB);
    static float cross(Vector vectorA, Vector vectorB);
    static float cross3(Vector vectorA, Vector vectorB, Vector vectorC);
    static float magnitude(Vector vector);
    static float magnitudeSquared(Vector vector);
    static Vector rotate(Vector vector, float angle, Vector output = nullptr);
    static Vector rotateAbout(Vector vector, float angle, Vector point, Vector output = nullptr);
    static Vector normalise(Vector vector);
    static float dot(Vector vectorA, Vector vectorB);
    std::string toString();
};

#endif
