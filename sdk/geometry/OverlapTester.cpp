#include <iostream>
#include <cmath>

#include "Vector.h"
#include "Circle.h"
#include "Sphere.h"
#include "Rectangle.h"


class OverlapTester {
public:
    static bool overlapCircles(const Circle& c1, const Circle& c2) {
        float distance = c1.center.distSquared(c2.center);
        float radiusSum = c1.radius + c2.radius;
        return distance <= radiusSum * radiusSum;
    }

    static bool overlapRectangles(const Rectangle& r1, const Rectangle& r2) {
        return (r1.lowerLeft.x < r2.lowerLeft.x + r2.width &&
                r1.lowerLeft.x + r1.width > r2.lowerLeft.x &&
                r1.lowerLeft.y < r2.lowerLeft.y + r2.height &&
                r1.lowerLeft.y + r1.height > r2.lowerLeft.y);
    }

    static bool overlapCircleRectangle(const Circle& c, const Rectangle& r) {
        float closestX = c.center.x;
        float closestY = c.center.y;

        if (c.center.x < r.lowerLeft.x)
            closestX = r.lowerLeft.x;
        else if (c.center.x > r.lowerLeft.x + r.width)
            closestX = r.lowerLeft.x + r.width;

        if (c.center.y < r.lowerLeft.y)
            closestY = r.lowerLeft.y;
        else if (c.center.y > r.lowerLeft.y + r.height)
            closestY = r.lowerLeft.y + r.height;

        return c.center.distSquared(closestX, closestY) < c.radius * c.radius;
    }

    static bool pointInCircle(const Circle& c, const Vector& p) {
        return c.center.distSquared(p) < c.radius * c.radius;
    }

    static bool pointInRectangle(const Rectangle& r, const Vector& p) {
        return (r.lowerLeft.x <= p.x && r.lowerLeft.x + r.width >= p.x &&
                r.lowerLeft.y <= p.y && r.lowerLeft.y + r.height >= p.y);
    }

    static bool overlapSpheres(const Sphere& s1, const Sphere& s2) {
        float distance = s1.center.distSquared(s2.center);
        float radiusSum = s1.radius + s2.radius;
        return distance <= radiusSum * radiusSum;
    }

    static bool pointInSphere(const Sphere& c, const Vector& p) {
        return c.center.distSquared(p) < c.radius * c.radius;
    }
};
