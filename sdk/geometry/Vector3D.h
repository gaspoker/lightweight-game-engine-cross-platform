#ifndef VECTOR3D_H
#define VECTOR3D_H

class Vector3D {
private:
    float matrix[16];
    float inVec[4];
    float outVec[4];

public:
    float x, y, z;

    Vector3D();
    Vector3D(float x, float y, float z);
    Vector3D(const Vector3D& other);

    Vector3D cpy() const;
    Vector3D& set(float x, float y, float z);
    Vector3D& set(const Vector3D& other);
    Vector3D& add(float x, float y, float z);
    Vector3D& add(const Vector3D& other);
    Vector3D& sub(float x, float y, float z);
    Vector3D& sub(const Vector3D& other);
    Vector3D& mul(float scalar);
    float len() const;
    Vector3D& nor();
    Vector3D& rotate(float angle, float axisX, float axisY, float axisZ);
    float dist(const Vector3D& other) const;
    float dist(float x, float y, float z) const;
    float distSquared(const Vector3D& other) const;
    float distSquared(float x, float y, float z) const;
};

#endif // VECTOR3D_H
