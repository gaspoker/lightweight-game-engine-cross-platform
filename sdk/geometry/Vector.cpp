#pragma once

class Vector {
public:
    static float TO_RADIANS;
    static float TO_DEGREES;
    float x, y;

    Vector() : x(0.0f), y(0.0f) {}

    Vector(float x, float y) : x(x), y(y) {}

    Vector(const Vector& other) : x(other.x), y(other.y) {}

    Vector cpy() {
        return Vector(x, y);
    }

    Vector set(float x, float y) {
        this->x = x;
        this->y = y;
        return *this;
    }

    Vector set(Vector other) {
        this->x = other.x;
        this->y = other.y;
        return *this;
    }

    Vector add(float x, float y) {
        this->x += x;
        this->y += y;
        return *this;
    }

    Vector add(Vector other) {
        this->x += other.x;
        this->y += other.y;
        return *this;
    }

    Vector sub(float x, float y) {
        this->x -= x;
        this->y -= y;
        return *this;
    }

    Vector sub(Vector other) {
        this->x -= other.x;
        this->y -= other.y;
        return *this;
    }

    Vector mult(float scalar) {
        this->x *= scalar;
        this->y *= scalar;
        return *this;
    }

    Vector div(float scalar) {
        this->x /= scalar;
        this->y /= scalar;
        return *this;
    }

    float len() {
        return sqrt(x * x + y * y);
    }

    Vector nor() {
        float len = len();
        if (len != 0.0f) {
            this->x /= len;
            this->y /= len;
        }
        return *this;
    }

    Vector rotate(float angle) {
        float rad = angle * TO_RADIANS;
        float cos = cosf(rad);
        float sin = sinf(rad);

        float newX = this->x * cos - this->y * sin;
        float newY = this->x * sin + this->y * cos;

        this->x = newX;
        this->y = newY;

        return *this;
    }

    float dist(Vector other) {
        float distX = this->x - other.x;
        float distY = this->y - other.y;
        return sqrt(distX * distX + distY * distY);
    }

    float dist(float x, float y) {
        float distX = this->x - x;
        float distY = this->y - y;
        return sqrt(distX * distX + distY * distY);
    }

    float distSquared(Vector other) {
        float distX = this->x - other.x;
        float distY = this->y - other.y;
        return distX * distX + distY * distY;
    }

    float distSquared(float x, float y) {
        float distX = this->x - x;
        float distY = this->y - y;
        return distX * distX + distY * distY;
    }

    static Vector create(float x, float y) {
        return Vector(x, y);
    }

    static Vector add(Vector vectorA, Vector vectorB, Vector output) {
        output = (output != nullptr) ? output : Vector();
        output.x = vectorA.x + vectorB.x;
        output.y = vectorA.y + vectorB.y;
        return output;
    }

    static Vector div(Vector vector, float scalar) {
        Vector output;
        output.x = vector.x / scalar;
        output.y = vector.y / scalar;
        return output;
    }

    static Vector perp(Vector vector, bool negate) {
        float mult = negate ? -1.0f : 1.0f;
        return Vector(mult * -vector.y, mult * vector.x);
    }

    static Vector neg(Vector vector) {
        return Vector(-vector.x, -vector.y);
    }

    static Vector mult(Vector vector, float scalar) {
        Vector output;
        output.x = vector.x * scalar;
        output.y = vector.y * scalar;
        return output;
    }

    static Vector sub(Vector vectorA, Vector vectorB, Vector output) {
        output = (output != nullptr) ? output : Vector();
        output.x = vectorA.x - vectorB.x;
        output.y = vectorA.y - vectorB.y;
        return output;
    }

    static float angle(Vector vectorA, Vector vectorB) {
        return atan2(vectorB.y - vectorA.y, vectorB.x - vectorA.x);
    }

    static float cross(Vector vectorA, Vector vectorB) {
        return (vectorA.x * vectorB.y) - (vectorA.y * vectorB.x);
    }

    static float cross3(Vector vectorA, Vector vectorB, Vector vectorC) {
        return (vectorB.x - vectorA.x) * (vectorC.y - vectorA.y) - (vectorB.y - vectorA.y) * (vectorC.x - vectorA.x);
    }

    static float magnitude(Vector vector) {
        return sqrt((vector.x * vector.x) + (vector.y * vector.y));
    }

    static float magnitudeSquared(Vector vector) {
        return (vector.x * vector.x) + (vector.y * vector.y);
    }

    static Vector rotate(Vector vector, float angle, Vector output) {
        output = (output != nullptr) ? output : Vector();
        float cos = cosf(angle);
        float sin = sinf(angle);
        float x = vector.x * cos - vector.y * sin;
        output.y = vector.x * sin + vector.y * cos;
        output.x = x;
        return output;
    }

    static Vector rotateAbout(Vector vector, float angle, Vector point, Vector output) {
        output = (output != nullptr) ? output : Vector();
        float cos = cosf(angle);
        float sin = sinf(angle);
        float x = point.x + ((vector.x - point.x) * cos - (vector.y - point.y) * sin);
        output.y = point.y + ((vector.x - point.x) * sin + (vector.y - point.y) * cos);
        output.x = x;
        return output;
    }

    static Vector normalise(Vector vector) {
        Vector output;
        float magnitude = Vector::magnitude(vector);
        if (magnitude == 0.0f) {
            return output;
        }
        output.y = vector.y / magnitude;
        output.x = vector.x / magnitude;
        return output;
    }

    static float dot(Vector vectorA, Vector vectorB) {
        return (vectorA.x * vectorB.x) + (vectorA.y * vectorB.y);
    }

    std::string toString() {
        return "[" + std::to_string(x) + ", " + std::to_string(y) + "]";
    }
};
