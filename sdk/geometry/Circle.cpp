#include <iostream>

#include "Vector.h"

class Circle {
public:
    Vector center;
    float radius;

    Circle(float x, float y, float radius) : center(x, y), radius(radius) {}
};
