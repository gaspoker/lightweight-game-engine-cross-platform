#include <cmath>
#include <android/opengl.h>
#include <android/matrix.h>

class Vector3D {
private:
    float matrix[16];
    float inVec[4];
    float outVec[4];

public:
    float x, y, z;

    Vector3D() : x(0), y(0), z(0) {}

    Vector3D(float x, float y, float z) : x(x), y(y), z(z) {}

    Vector3D(const Vector3D& other) : x(other.x), y(other.y), z(other.z) {}

    Vector3D cpy() const {
        return Vector3D(x, y, z);
    }

    Vector3D& set(float x, float y, float z) {
        this->x = x;
        this->y = y;
        this->z = z;
        return *this;
    }

    Vector3D& set(const Vector3D& other) {
        x = other.x;
        y = other.y;
        z = other.z;
        return *this;
    }

    Vector3D& add(float x, float y, float z) {
        this->x += x;
        this->y += y;
        this->z += z;
        return *this;
    }

    Vector3D& add(const Vector3D& other) {
        x += other.x;
        y += other.y;
        z += other.z;
        return *this;
    }

    Vector3D& sub(float x, float y, float z) {
        this->x -= x;
        this->y -= y;
        this->z -= z;
        return *this;
    }

    Vector3D& sub(const Vector3D& other) {
        x -= other.x;
        y -= other.y;
        z -= other.z;
        return *this;
    }

    Vector3D& mul(float scalar) {
        x *= scalar;
        y *= scalar;
        z *= scalar;
        return *this;
    }

    float len() const {
        return sqrt(x * x + y * y + z * z);
    }

    Vector3D& nor() {
        float length = len();
        if (length != 0) {
            x /= length;
            y /= length;
            z /= length;
        }
        return *this;
    }

    Vector3D& rotate(float angle, float axisX, float axisY, float axisZ) {
        inVec[0] = x;
        inVec[1] = y;
        inVec[2] = z;
        inVec[3] = 1;
        Matrix::setIdentityM(matrix, 0);
        Matrix::rotateM(matrix, 0, angle, axisX, axisY, axisZ);
        Matrix::multiplyMV(outVec, 0, matrix, 0, inVec, 0);
        x = outVec[0];
        y = outVec[1];
        z = outVec[2];
        return *this;
    }

    float dist(const Vector3D& other) const {
        float distX = x - other.x;
        float distY = y - other.y;
        float distZ = z - other.z;
        return sqrt(distX * distX + distY * distY + distZ * distZ);
    }

    float dist(float x, float y, float z) const {
        float distX = this->x - x;
        float distY = this->y - y;
        float distZ = this->z - z;
        return sqrt(distX * distX + distY * distY + distZ * distZ);
    }

    float distSquared(const Vector3D& other) const {
        float distX = x - other.x;
        float distY = y - other.y;
        float distZ = z - other.z;
        return distX * distX + distY * distY + distZ * distZ;
    }

    float distSquared(float x, float y, float z) const {
        float distX = this->x - x;
        float distY = this->y - y;
        float distZ = this->z - z;
        return distX * distX + distY * distY + distZ * distZ;
    }
};
