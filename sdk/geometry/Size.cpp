#include <iostream>

class Size {
public:
    float width, height;
    float aspectRatio;

    Size() : width(0), height(0), aspectRatio(1) {}

    Size(float w, float h) : width(w), height(h) {
        aspectRatio = (height == 0) ? 1 : width / height;
    }

    static Size make(float w, float h) {
        return Size(w, h);
    }

    static Size zero() {
        return Size(0, 0);
    }

    float getWidth() const {
        return width;
    }

    float getHeight() const {
        return height;
    }

    static bool equalToSize(const Size& s1, const Size& s2) {
        return s1.width == s2.width && s1.height == s2.height;
    }

    std::string toString() const {
        return "<" + std::to_string(width) + ", " + std::to_string(height) + ">";
    }
};
