#pragma once

#include <vector>
#include <limits>
#include <cmath>

#include "geometry/Vertex.h"
#include "geometry/Vector.h"

class Bounds {
public:
    Vertex min;
    Vertex max;

    Bounds(float minX, float minY, float maxX, float maxY) : min(minX, minY), max(maxX, maxY) {}

    Bounds() : min(0.0f, 0.0f), max(0.0f, 0.0f) {}

    Bounds(const std::vector<Vertex>& vertices) : min(std::numeric_limits<float>::infinity(), std::numeric_limits<float>::infinity()),
                                                  max(-std::numeric_limits<float>::infinity(), -std::numeric_limits<float>::infinity()) {
        update(vertices, nullptr);
    }

    void update(const std::vector<Vertex>& vertices, const Vector* velocity) {
        min.x = std::numeric_limits<float>::infinity();
        min.y = std::numeric_limits<float>::infinity();
        max.x = -std::numeric_limits<float>::infinity();
        max.y = -std::numeric_limits<float>::infinity();

        for (const Vertex& vertex : vertices) {
            if (vertex.x > max.x) max.x = vertex.x;
            if (vertex.x < min.x) min.x = vertex.x;
            if (vertex.y > max.y) max.y = vertex.y;
            if (vertex.y < min.y) min.y = vertex.y;
        }

        if (velocity != nullptr) {
            if (velocity->x > 0.0f) {
                max.x += velocity->x;
            } else {
                min.x += velocity->x;
            }

            if (velocity->y > 0.0f) {
                max.y += velocity->y;
            } else {
                min.y += velocity->y;
            }
        }
    }

    bool contains(const Vector& point) const {
        return point.x >= min.x && point.x <= max.x && point.y >= min.y && point.y <= max.y;
    }

    static bool overlaps(const Bounds& boundsA, const Bounds& boundsB) {
        return (boundsA.min.x <= boundsB.max.x && boundsA.max.x >= boundsB.min.x &&
                boundsA.max.y >= boundsB.min.y && boundsA.min.y <= boundsB.max.y);
    }

    void translate(const Vector& vector) {
        min.x += vector.x;
        max.x += vector.x;
        min.y += vector.y;
        max.y += vector.y;
    }

    void shift(const Vector& position) {
        float deltaX = max.x - min.x;
        float deltaY = max.y - min.y;

        min.x = position.x;
        max.x = position.x + deltaX;
        min.y = position.y;
        max.y = position.y + deltaY;
    }
};
