#pragma once

#include "Vector.h"
#include "Vertex.h"

#include <vector>
#include <limits>

class Bounds {
public:
    Vertex min;
    Vertex max;

    Bounds(float minX, float minY, float maxX, float maxY);
    Bounds();
    Bounds(const std::vector<Vertex>& vertices);

    void update(const std::vector<Vertex>& vertices, const Vector* velocity);

    bool contains(const Vector& point) const;

    static bool overlaps(const Bounds& boundsA, const Bounds& boundsB);

    void translate(const Vector& vector);

    void shift(const Vector& position);
};
