#include "Vector.h"

class Rectangle {
public:
    Vector lowerLeft;
    float width, height;
    
    Rectangle(float x, float y, float width, float height) {
        set(x, y, width, height);
    }

    void set(float x, float y, float width, float height) {
        this->lowerLeft = Vector(x, y);
        this->width = width;
        this->height = height;
    }

    static Rectangle add(const Rectangle& rect, float dx, float dy) {
        Rectangle output(rect.lowerLeft.x, rect.lowerLeft.y, rect.width, rect.height);
        output.lowerLeft.add(dx, dy);
        return output;
    }

    /*void addX(float dx) {
        this->lowerLeft.add(dx, 0);
    }

    void addY(float dy) {
        this->lowerLeft.add(0, dy);
    }*/
};
