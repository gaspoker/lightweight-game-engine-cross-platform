#pragma once

#include <vector>
#include <cmath>

class Vector {
public:
    float x;
    float y;

    Vector(float x, float y) : x(x), y(y) {}

    static Vector normalise(const Vector& v) {
        float length = std::sqrt(v.x * v.x + v.y * v.y);
        if (length != 0.0f)
            return Vector(v.x / length, v.y / length);
        else
            return Vector(0.0f, 0.0f);
    }
};

class Axes {
public:
    static std::vector<Vector> fromVertices(const std::vector<Vector>& vertices) {
        std::vector<Vector> axes;

        for (size_t i = 0; i < vertices.size(); i++) {
            size_t j = (i + 1) % vertices.size();
            Vector normal = Vector::normalise(Vector(vertices[j].y - vertices[i].y, vertices[i].x - vertices[j].x));
            axes.push_back(normal);
        }

        return axes;
    }

    static void rotate(std::vector<Vector>& axes, float angle) {
        if (angle != 0.0f) {
            float cos = std::cos(angle);
            float sin = std::sin(angle);

            for (size_t i = 0; i < axes.size(); i++) {
                Vector& axis = axes[i];
                float aux = axis.x * cos - axis.y * sin;
                axis.y = axis.x * sin + axis.y * cos;
                axis.x = aux;
            }
        }
    }
};
