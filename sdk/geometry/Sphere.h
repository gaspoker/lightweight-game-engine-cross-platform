#pragma once

#include "Vector3D.h"

class Sphere {
public:
    Vector3D center;
    float radius;

    Sphere(float x, float y, float z, float radius);
};
