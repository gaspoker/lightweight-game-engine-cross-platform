#include <vector>
#include <cmath>
#include <string>

class Vector {
public:
    float x, y;
    Vector(float x, float y) : x(x), y(y) {}
};

class Vertex {
public:
    Vector position;
    int index;
    bool isInternal;

    Vertex(Vector position) : position(position), index(0), isInternal(false) {}
};

class Body {
};

class Vertices {
private:
    static float cross(const Vector& a, const Vector& b) {
        return a.x * b.y - a.y * b.x;
    }

public:
    static std::vector<Vertex> fromVectors(const std::vector<Vector>& points, Body* body) {
        std::vector<Vertex> vertices;
        for (size_t i = 0; i < points.size(); i++) {
            Vertex vertex(points[i]);
            vertex.index = i;
            vertex.isInternal = false;
            vertices.push_back(vertex);
        }
        return vertices;
    }

    static std::vector<Vertex> fromPath(const std::string& path, Body* body) {
    }

    static Vector centre(const std::vector<Vertex>& vertices) {
        float area = 0;
        Vector centre(0, 0);

        for (size_t i = 0; i < vertices.size(); i++) {
            size_t j = (i + 1) % vertices.size();
            float cross = Vertices::cross(vertices[i].position, vertices[j].position);

            Vector temp((vertices[i].position.x + vertices[j].position.x) * cross,
                        (vertices[i].position.y + vertices[j].position.y) * cross);

            centre.x += temp.x;
            centre.y += temp.y;
        }

        centre.x /= (6 * area);
        centre.y /= (6 * area);

        return centre;
    }

    static Vertex mean(const std::vector<Vertex>& vertices) {
        Vertex average(Vector(0, 0));

        for (const Vertex& vertex : vertices) {
            average.position.x += vertex.position.x;
            average.position.y += vertex.position.y;
        }
        average.position.x /= vertices.size();
        average.position.y /= vertices.size();

        return average;
    }

    static float area(const std::vector<Vertex>& vertices) {
        float area = 0;
        size_t j = vertices.size() - 1;
        for (size_t i = 0; i < vertices.size(); i++) {
            area += (vertices[j].position.x - vertices[i].position.x) * (vertices[j].position.y + vertices[i].position.y);
            j = i;
        }

        return std::abs(area) / 2;
    }

    static float inertia(const std::vector<Vertex>& vertices, float mass) {
        float numerator = 0;
        float denominator = 0;

        for (size_t n = 0; n < vertices.size(); n++) {
            size_t j = (n + 1) % vertices.size();
            float cross = std::abs(Vertices::cross(vertices[j].position, vertices[n].position));
            numerator += cross * (vertices[j].position.x * vertices[j].position.x + 
                                  vertices[j].position.y * vertices[j].position.y + 
                                  vertices[j].position.x * vertices[n].position.x + 
                                  vertices[j].position.y * vertices[n].position.y +
                                  vertices[n].position.x * vertices[n].position.x + 
                                  vertices[n].position.y * vertices[n].position.y);
            denominator += cross;
        }

        return (mass / 6) * (numerator / denominator);
    }

    static std::vector<Vertex> translate(std::vector<Vertex>& vertices, const Vector& vector, float scalar = 1.0f) {
        for (Vertex& vertex : vertices) {
            vertex.position.x += vector.x * scalar;
            vertex.position.y += vector.y * scalar;
        }
        return vertices;
    }

    static std::vector<Vertex> rotate(std::vector<Vertex>& vertices, float angle, const Vector& point) {
        if (angle == 0)
            return vertices;

        float cosAngle = std::cos(angle);
        float sinAngle = std::sin(angle);

        for (Vertex& vertex : vertices) {
            float dx = vertex.position.x - point.x;
            float dy = vertex.position.y - point.y;

            vertex.position.x = point.x + (dx * cosAngle - dy * sinAngle);
            vertex.position.y = point.y + (dx * sinAngle + dy * cosAngle);
        }

        return vertices;
    }

    static bool contains(const std::vector<Vertex>& vertices, const Vertex& point) {
        for (size_t i = 0; i < vertices.size(); i++) {
            const Vertex& vertex = vertices[i];
            const Vertex& nextVertex = vertices[(i + 1) % vertices.size()];

            if ((point.position.x - vertex.position.x) * (nextVertex.position.y - vertex.position.y) + 
                (point.position.y - vertex.position.y) * (vertex.position.x - nextVertex.position.x) > 0) {
                return false;
            }
        }

        return true;
    }

    static std::vector<Vertex> scale(std::vector<Vertex>& vertices, float scaleX, float scaleY, const Vector& point = nullptr) {
        Vector centre = (point == nullptr) ? centre(vertices) : *point;

        for (Vertex& vertex : vertices) {
            Vector delta(vertex.position.x - centre.x, vertex.position.y - centre.y);
            vertex.position.x = centre.x + delta.x * scaleX;
            vertex.position.y = centre.y + delta.y * scaleY;
        }

        return vertices;
    }

    static std::vector<Vertex> hull(std::vector<Vertex>& vertices) {
        std::vector<Vertex> upper;
        std::vector<Vertex> lower;

        std::vector<Vertex> cpyVertices = vertices;
        std::sort(cpyVertices.begin(), cpyVertices.end(), [](const Vertex& a, const Vertex& b) {
            float dx = a.position.x - b.position.x;
            return (dx != 0 ? dx < 0 : a.position.y < b.position.y);
        });

        for (size_t i = 0; i < cpyVertices.size(); i++) {
            const Vertex& vertex = cpyVertices[i];
            while (lower.size() >= 2 && cross3(lower[lower.size() - 2], lower[lower.size() - 1], vertex) <= 0) {
                lower.pop_back();
            }
            lower.push_back(vertex);
        }

        for (size_t i = cpyVertices.size() - 1; i >= 0; i--) {
            const Vertex& vertex = cpyVertices[i];
            while (upper.size() >= 2 && cross3(upper[upper.size() - 2], upper[upper.size() - 1], vertex) <= 0) {
                upper.pop_back();
            }
            upper.push_back(vertex);
        }

        upper.pop_back();
        lower.pop_back();
        upper.insert(upper.end(), lower.begin(), lower.end());

        return upper;
    }

// Helpers
private:
    static float cross3(const Vertex& p1, const Vertex& p2, const Vertex& p3) {
        float dx1 = p2.position.x - p1.position.x;
        float dy1 = p2.position.y - p1.position.y;
        float dx2 = p3.position.x - p2.position.x;
        float dy2 = p3.position.y - p2.position.y;
        return dx1 * dy2 - dx2 * dy1;
    }
};
