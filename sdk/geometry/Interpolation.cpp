#include <cmath>

class Interpolation {
public:
    /**
     * A linear interpolation method.
     *
     * @param v The input array of values to interpolate between.
     * @param k The percentage of interpolation, between 0 and 1.
     *
     * @return The interpolated value.
     */
    static float getLinearValue(const short* v, float k) {
        int m = sizeof(v) / sizeof(v[0]) - 1;
        float f = m * k;
        int i = static_cast<int>(std::floor(f));

        if (k < 0.0f) {
            return linear(v[0], v[1], f);
        } else if (k > 1.0f) {
            return linear(v[m], v[m - 1], m - f);
        } else {
            return linear(v[i], v[std::min(i + 1, m)], f - i);
        }
    }

    /**
     * A bezier interpolation method.
     *
     * @param v The input array of values to interpolate between.
     * @param k The percentage of interpolation, between 0 and 1.
     *
     * @return The interpolated value.
     */
    static float getBezierValue(const int* v, float k) {
        float b = 0.0f;
        int n = sizeof(v) / sizeof(v[0]) - 1;

        for (int i = 0; i <= n; i++) {
            b += std::pow(1.0f - k, n - i) * std::pow(k, i) * v[i] * bernstein(n, i);
        }

        return b;
    }

private:
    /**
     * Calculates a linear (interpolation) value over t.
     *
     * @param p0 The first point.
     * @param p1 The second point.
     * @param t The percentage between p0 and p1 to return, represented as a number between 0 and 1.
     *
     * @return The step t% of the way between p0 and p1.
     */
    static float linear(float p0, float p1, float t) {
        return (p1 - p0) * t + p0;
    }

    /**
     * Calculates the factorial of a given number for integer values greater than 0.
     *
     * @param value A positive integer to calculate the factorial of.
     *
     * @return The factorial of the given number.
     */
    static int factorial(int value) {
        if (value == 0) {
            return 1;
        }

        int res = value;

        while (--value != 0) {
            res *= value;
        }

        return res;
    }

    /**
     * [description]
     *
     * @param n [description]
     * @param i [description]
     *
     * @return [description]
     */
    static float bernstein(int n, int i) {
        return static_cast<float>(factorial(n)) / (factorial(i) * factorial(n - i));
    }
};
