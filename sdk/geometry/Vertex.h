#pragma once

#include "Vector.h"

class Body; // Debes proporcionar la definición de la clase Body
class Contact; // Debes proporcionar la definición de la clase Contact

class Vertex : public Vector {
public:
    bool isInternal;
    int index;
    Body* body;
    Contact* contact;
    float offset;

    Vertex();
    Vertex(float x, float y);
    Vertex(const Vector& point);

    // Clase Contact interna
    class Contact {
    public:
        Vertex* vertex;
        float normalImpulse;
        float tangentImpulse;

        Contact(Vertex* vertex, float normalImpulse, float tangentImpulse);
    };
};

#endif
