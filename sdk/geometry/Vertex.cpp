#pragma once

#include "Vector.hpp"


class Vertex : public Vector {
public:
    bool isInternal;
    int index;
    Body* body;
    Contact* contact;
    float offset;

    Vertex() : Vector(), isInternal(false), index(0), body(nullptr), contact(nullptr), offset(0.0f) {}

    Vertex(float x, float y) : Vector(x, y), isInternal(false), index(0), body(nullptr), contact(nullptr), offset(0.0f) {}

    Vertex(const Vector& point) : Vector(point), isInternal(false), index(0), body(nullptr), contact(nullptr), offset(0.0f) {}

    // Clase Contact interna
    class Contact {
    public:
        Vertex* vertex;
        float normalImpulse;
        float tangentImpulse;

        Contact(Vertex* vertex, float normalImpulse, float tangentImpulse) : vertex(vertex), normalImpulse(normalImpulse), tangentImpulse(tangentImpulse) {}
    };
};
