#pragma once

#include "Vector.h"

class Circle {
public:
    Vector center;
    float radius;

    Circle(float x, float y, float radius);
};
