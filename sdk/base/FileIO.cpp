#include "FileIO.h"

#include "Context.h"
#include "SharedPreferences.h"
#include <fstream>

FileIO::FileIO(Context* context) {
    this->context = context;
}

std::istream* FileIO::readAsset(std::string fileName) {
    return nullptr; // TODO
}

std::istream* FileIO::readFile(std::string fileName) {
    std::ifstream* fileStream = new std::ifstream(externalStoragePath + fileName);
    return fileStream;
}

std::ostream* FileIO::writeFile(std::string fileName) {
    std::ofstream* fileStream = new std::ofstream(externalStoragePath + fileName);
    return fileStream;
}

SharedPreferences* FileIO::getPreferences() {
    return context->getSharedPreferences();
}
