#ifndef SINGLE_TOUCH_HANDLER_H
#define SINGLE_TOUCH_HANDLER_H

#include <vector>
#include <memory>
#include <functional>

class TouchEvent {
public:
    enum Type {
        TOUCH_DOWN,
        TOUCH_UP,
        TOUCH_DRAGGED,
        TOUCH_OUT,
        DRAG_NONE,
        DRAG_UP,
        DRAG_DOWN,
        DRAG_LEFT,
        DRAG_RIGHT
    };

    Type type;
    Type dragDirection;
    long long dragDuration;
    int x;
    int y;
};

class SingleTouchHandler {
public:
    using PoolObjectFactory = std::function<std::shared_ptr<TouchEvent>()>;

    SingleTouchHandler(float scaleX, float scaleY) 
        : isTouched(false), touchX(0), touchY(0), scaleX(scaleX), scaleY(scaleY) {}

    void onTouch(int action, float x, float y) {
        auto touchEvent = touchEventPool.newObject();
        switch (action) {
            case 0: // ACTION_DOWN
                touchEvent->type = TouchEvent::TOUCH_DOWN;
                isTouched = true;
                initialX = x;
                initialY = y;
                startDragging = getCurrentTimeMillis();
                break;
            case 1: // ACTION_UP
            case 3: // ACTION_CANCEL
                touchEvent->type = TouchEvent::TOUCH_UP;
                isTouched = false;
                break;
            case 4: // ACTION_OUTSIDE
                touchEvent->type = TouchEvent::TOUCH_OUT;
                isTouched = false;
                break;
            case 2: // ACTION_MOVE
                touchEvent->type = TouchEvent::TOUCH_DRAGGED;
                int currentX = x * scaleX;
                int currentY = y * scaleY;
                int offsetX = currentX - initialX;
                int offsetY = currentY - initialY;
                if (std::abs(offsetX) > slop) {
                    if (offsetX > TouchEvent::DEFAULT_THRESHOLD) {
                        touchEvent->dragDirection = TouchEvent::DRAG_RIGHT;
                    } else if (offsetX < -TouchEvent::DEFAULT_THRESHOLD) {
                        touchEvent->dragDirection = TouchEvent::DRAG_LEFT;
                    }
                }
                if (std::abs(offsetY) > slop) {
                    if (offsetY > TouchEvent::DEFAULT_THRESHOLD) {
                        touchEvent->dragDirection = TouchEvent::DRAG_DOWN;
                    } else if (offsetY < -TouchEvent::DEFAULT_THRESHOLD) {
                        touchEvent->dragDirection = TouchEvent::DRAG_UP;
                    }
                }
                isTouched = true;
                break;
        }

        touchEvent->x = touchX = static_cast<int>(x * scaleX);
        touchEvent->y = touchY = static_cast<int>(y * scaleY);
        touchEventsBuffer.push_back(touchEvent);
    }

    bool isTouchDown(int pointer) const {
        if (pointer == 0)
            return isTouched;
        else
            return false;
    }

    int getTouchX(int pointer) const {
        return touchX;
    }

    int getTouchY(int pointer) const {
        return touchY;
    }

    const std::vector<std::shared_ptr<TouchEvent>>& getTouchEvents() const {
        return touchEvents;
    }

private:
    bool isTouched;
    int touchX;
    int touchY;
    std::vector<std::shared_ptr<TouchEvent>> touchEvents;
    std::vector<std::shared_ptr<TouchEvent>> touchEventsBuffer;
    float scaleX;
    float scaleY;
    const float slop = 10.0f;
    float initialX = 0.0f;
    float initialY = 0.0f;
    long long startDragging = 0;

    // Function to get current time in milliseconds
    long long getCurrentTimeMillis() const {
        // Implement this function according to the platform
        // For example, on Windows use GetTickCount64
        // On Unix-based systems use std::chrono::system_clock::now()
        return 0;
    }

    PoolObjectFactory touchEventPool = []() { return std::make_shared<TouchEvent>(); };
};

#endif // SINGLE_TOUCH_HANDLER_H
