#ifndef ONKEYLISTENER_H
#define ONKEYLISTENER_H

class View;

class OnKeyListener {
public:
    virtual bool onKey(View* v, int keyCode, android::view::KeyEvent* event) = 0;
};

#endif // ONKEYLISTENER_H
