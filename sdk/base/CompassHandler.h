#ifndef COMPASSHANDLER_H
#define COMPASSHANDLER_H

class Context;
class Sensor;
class SensorEvent;
class SensorManager;

class CompassHandler {
private:
    float yaw;
    float pitch;
    float roll;

public:
    CompassHandler(Context* context);

    void onAccuracyChanged(Sensor* sensor, int accuracy);

    void onSensorChanged(SensorEvent* event);

    float getYaw();

    float getPitch();

    float getRoll();
};

#endif // COMPASSHANDLER_H
