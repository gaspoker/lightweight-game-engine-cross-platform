#ifndef FILEIO_H
#define FILEIO_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

class Context;
class SharedPreferences;

class FileIO {
private:
    Context* context;
    std::string externalStoragePath;

public:
    FileIO(Context* context);

    std::istream* readAsset(std::string fileName);

    std::istream* readFile(std::string fileName);

    std::ostream* writeFile(std::string fileName);

    SharedPreferences* getPreferences();
};

#endif // FILEIO_H
