#ifndef KEYBOARDHANDLER_H
#define KEYBOARDHANDLER_H

#include "KeyEvent.h"
#include "Pool.h"
#include "View.h"
#include "OnKeyListener.h"
#include <vector>
#include <iostream>
#include <algorithm>

class View;

class KeyboardHandler : public OnKeyListener {
private:
    std::vector<bool> pressedKeys;
    Pool<KeyEvent>* keyEventPool;
    std::vector<KeyEvent*> keyEventsBuffer;
    std::vector<KeyEvent*> keyEvents;

public:
    KeyboardHandler(View* view) {
        pressedKeys.resize(128, false);
        PoolObjectFactory<KeyEvent>* factory = new PoolObjectFactory<KeyEvent>() {
            KeyEvent* createObject() override {
                return new KeyEvent();
            }
        };
        keyEventPool = new Pool<KeyEvent>(factory, 100);
        view->setOnKeyListener(this);
        view->setFocusableInTouchMode(true);
        view->requestFocus();
    }

    ~KeyboardHandler() {
        delete keyEventPool;
        for (auto event : keyEventsBuffer)
            delete event;
        for (auto event : keyEvents)
            delete event;
    }

    bool onKey(View* v, int keyCode, android::view::KeyEvent* event) override {
        if (event->getAction() == android::view::KeyEvent::ACTION_MULTIPLE)
            return false;

        synchronized (this) {
            KeyEvent* keyEvent = keyEventPool->newObject();
            keyEvent->keyCode = keyCode;
            keyEvent->keyChar = static_cast<char>(event->getUnicodeChar());
            if (event->getAction() == android::view::KeyEvent::ACTION_DOWN) {
                keyEvent->type = KeyEvent::KEY_DOWN;
                if (keyCode > 0 && keyCode < 127)
                    pressedKeys[keyCode] = true;
            }
            if (event->getAction() == android::view::KeyEvent::ACTION_UP) {
                keyEvent->type = KeyEvent::KEY_UP;
                if (keyCode > 0 && keyCode < 127)
                    pressedKeys[keyCode] = false;
            }
            keyEventsBuffer.push_back(keyEvent);
        }
        return false;
    }

    bool isKeyPressed(int keyCode) {
        if (keyCode < 0 || keyCode > 127)
            return false;
        return pressedKeys[keyCode];
    }

    std::vector<KeyEvent*> getKeyEvents() {
        synchronized (this) {
            int len = keyEvents.size();
            for (int i = 0; i < len; i++) {
                keyEventPool->free(keyEvents[i]);
            }
            keyEvents.clear();
            keyEvents.insert(keyEvents.end(), keyEventsBuffer.begin(), keyEventsBuffer.end());
            keyEventsBuffer.clear();
            return keyEvents;
        }
    }
};

#endif // KEYBOARDHANDLER_H
