#ifndef IMUSIC_H
#define IMUSIC_H

class IMusic {
public:
    virtual void play() = 0;
    virtual void play(float volume, bool isLooping) = 0;
    virtual void stop() = 0;
    virtual void pause() = 0;
    virtual void setLooping(bool looping) = 0;
    virtual void setVolume(float volume) = 0;
    virtual bool isPlaying() = 0;
    virtual bool isStopped() = 0;
    virtual bool isLooping() = 0;
    virtual void dispose() = 0;
};

#endif // IMUSIC_H
