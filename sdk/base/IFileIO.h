#ifndef IFILEIO_H
#define IFILEIO_H

#include <iostream>

class InputStream;
class OutputStream;

class IFileIO {
public:
    virtual InputStream* readAsset(std::string fileName) = 0;
    virtual InputStream* readFile(std::string fileName) = 0;
    virtual OutputStream* writeFile(std::string fileName) = 0;
};

#endif // IFILEIO_H
