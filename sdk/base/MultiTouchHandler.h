#ifndef MULTITOUCHHANDLER_H
#define MULTITOUCHHANDLER_H

#include <View.h>
#include <MotionEvent.h>
#include <IInput.h>

class MultiTouchHandler : public ITouchHandler {
private:
    static const int MAX_TOUCHPOINTS = 10;
    bool isTouched[MAX_TOUCHPOINTS];
    int touchX[MAX_TOUCHPOINTS];
    int touchY[MAX_TOUCHPOINTS];
    Pool<TouchEvent>* touchEventPool;
    std::vector<TouchEvent*> touchEventsBuffer;
    std::vector<TouchEvent*> touchEvents;

public:
    MultiTouchHandler(View* view, float scaleX, float scaleY);
    ~MultiTouchHandler();
    virtual bool onTouch(View* v, MotionEvent* event) override;
    virtual bool isTouchDown(int pointer) override;
    virtual int getTouchX(int pointer) override;
    virtual int getTouchY(int pointer) override;
    virtual std::vector<TouchEvent*> getTouchEvents() override;
};

#endif // MULTITOUCHHANDLER_H
