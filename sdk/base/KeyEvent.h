#ifndef KEYEVENT_H
#define KEYEVENT_H

class KeyEvent {
public:
    static const int KEY_DOWN = 0;
    static const int KEY_UP = 1;

    int type;
    int keyCode;
    char keyChar;
};

#endif // KEYEVENT_H
