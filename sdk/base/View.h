#ifndef VIEW_H
#define VIEW_H

class View {
public:
    void setOnKeyListener(OnKeyListener* listener);
    void setFocusableInTouchMode(bool focusable);
    void requestFocus();
};

#endif // VIEW_H
