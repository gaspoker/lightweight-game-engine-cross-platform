#ifndef ACCELEROMETERHANDLER_H
#define ACCELEROMETERHANDLER_H

#include "Context.h" // TODO
#include "Sensor.h" // TODO
#include "SensorManager.h" // TODO
#include "SensorEvent.h" // TODO
#include "SensorEventListener.h" // TODO

class AccelerometerHandler : public SensorEventListener {
private:
    float accelX;
    float accelY;
    float accelZ;

public:
    AccelerometerHandler(Context* context);
    virtual void onAccuracyChanged(Sensor* sensor, int accuracy) override;
    virtual void onSensorChanged(SensorEvent* event) override;

    float getAccelX();
    float getAccelY();
    float getAccelZ();
};

#endif // ACCELEROMETERHANDLER_H
