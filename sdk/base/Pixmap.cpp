#include "Pixmap.h"

Pixmap::Pixmap(Bitmap* bmp, PixmapFormat fmt) : bitmap(bmp), format(fmt) {}

int Pixmap::getWidth() const {
    return bitmap->getWidth();
}

int Pixmap::getHeight() const {
    return bitmap->getHeight();
}

PixmapFormat Pixmap::getFormat() const {
    return format;
}

void Pixmap::dispose() {
    if (bitmap != nullptr) {
        bitmap->recycle();
        delete bitmap;
        bitmap = nullptr;
    }
}
