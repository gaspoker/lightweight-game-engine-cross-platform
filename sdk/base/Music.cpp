#include "IMusic.h"

Music::Music(MediaPlayer* mp, AssetFileDescriptor* assetDescriptor) : mediaPlayer(mp) {
    try {
        mediaPlayer->setDataSource(assetDescriptor->getFileDescriptor(),
                                   assetDescriptor->getStartOffset(),
                                   assetDescriptor->getLength());
        mediaPlayer->prepare();
        isPrepared = true;
        mediaPlayer->setOnCompletionListener(this);
    } catch (Exception& e) {
        throw std::runtime_error("Couldn't load music");
    }
}

void Music::dispose() {
    if (mediaPlayer->isPlaying()) {
        mediaPlayer->stop();
    }
    mediaPlayer->release();
}

bool Music::isLooping() {
    return mediaPlayer->isLooping();
}

bool Music::isPlaying() {
    return mediaPlayer->isPlaying();
}

bool Music::isStopped() {
    return !isPrepared;
}

void Music::pause() {
    if (mediaPlayer->isPlaying()) {
        mediaPlayer->pause();
    }
}

void Music::play() {
    if (mediaPlayer->isPlaying()) {
        return;
    }
    try {
        std::lock_guard<std::mutex> lock(mutex);
        if (!isPrepared) {
            mediaPlayer->prepare();
        }
        mediaPlayer->start();
    } catch (IllegalStateException& e) {
        e.printStackTrace();
    } catch (IOException& e) {
        e.printStackTrace();
    }
}

void Music::play(float volume, bool isLooping) {
    mediaPlayer->setVolume(volume, volume);
    mediaPlayer->setLooping(isLooping);

    if (mediaPlayer->isPlaying()) {
        return;
    }
    try {
        std::lock_guard<std::mutex> lock(mutex);
        if (!isPrepared) {
            mediaPlayer->prepare();
        }
        mediaPlayer->start();
    } catch (IllegalStateException& e) {
        e.printStackTrace();
    } catch (IOException& e) {
        e.printStackTrace();
    }
}

void Music::setLooping(bool isLooping) {
    mediaPlayer->setLooping(isLooping);
}

void Music::setVolume(float volume) {
    mediaPlayer->setVolume(volume, volume);
}

void Music::stop() {
    mediaPlayer->stop();
    std::lock_guard<std::mutex> lock(mutex);
    isPrepared = false;
}

void Music::onCompletion(MediaPlayer* player) {
    stop();
}
