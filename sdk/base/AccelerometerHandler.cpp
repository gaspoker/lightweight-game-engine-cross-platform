#include <iostream>
#include <cmath>

class AccelerometerHandler {
private:
    float accelX;
    float accelY;
    float accelZ;

public:
    AccelerometerHandler() {
        // Constructor
    }

    void onSensorChanged(float x, float y, float z) {
        // Manejo de cambios en el sensor
        accelX = x;
        accelY = y;
        accelZ = z;
    }

    float getAccelX() {
        // Obtener el valor del acelerómetro en el eje X
        return accelX;
    }

    float getAccelY() {
        // Obtener el valor del acelerómetro en el eje Y
        return accelY;
    }

    float getAccelZ() {
        // Obtener el valor del acelerómetro en el eje Z
        return accelZ;
    }
};
