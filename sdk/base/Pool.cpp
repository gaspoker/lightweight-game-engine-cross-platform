#ifndef POOL_H
#define POOL_H

#include <vector>
#include <memory>

template<typename T>
class Pool {
public:
    using PoolObjectFactory = std::function<std::shared_ptr<T>()>;

    Pool(PoolObjectFactory factory, int maxSize) 
        : factory(factory), maxSize(maxSize), freeObjects(maxSize) {}

    std::shared_ptr<T> newObject() {
        std::shared_ptr<T> object = nullptr;

        if (freeObjects.empty()) {
            object = factory();
        } else {
            object = freeObjects.back();
            freeObjects.pop_back();
        }
        return object;
    }

    void free(std::shared_ptr<T> object) {
        if (freeObjects.size() < maxSize) {
            freeObjects.push_back(object);
        }
    }

private:
    PoolObjectFactory factory;
    int maxSize;
    std::vector<std::shared_ptr<T>> freeObjects;
};

#endif // POOL_H
