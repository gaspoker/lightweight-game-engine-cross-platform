#ifndef KEYBOARDHANDLER_H
#define KEYBOARDHANDLER_H

#include <vector>
#include <android/view/View.h>
#include <android/view/KeyEvent.h>
#include <sdk/base/IInput.h>

class KeyboardHandler : public OnKeyListener {
private:
    std::vector<bool> pressedKeys;
    Pool<KeyEvent>* keyEventPool;
    std::vector<KeyEvent*> keyEventsBuffer;
    std::vector<KeyEvent*> keyEvents;

public:
    KeyboardHandler(View* view);
    ~KeyboardHandler();
    virtual bool onKey(View* v, int keyCode, android::view::KeyEvent* event) override;
    bool isKeyPressed(int keyCode);
    std::vector<KeyEvent*> getKeyEvents();
};

#endif // KEYBOARDHANDLER_H
