#include "SpatialHashGrid.h"

SpatialHashGrid::SpatialHashGrid(float worldWidth, float worldHeight, float cellSize) {
    this->cellSize = cellSize;
    this->cellsPerRow = static_cast<int>(ceil(worldWidth / cellSize));
    this->cellsPerCol = static_cast<int>(ceil(worldHeight / cellSize));
    int numCells = cellsPerRow * cellsPerCol;
    dynamicCells.resize(numCells);
    staticCells.resize(numCells);
    cellIds.resize(4);
}

void SpatialHashGrid::insertStaticObject(GameObject* obj) {
    cellIds = getCellIds(obj);
    for (int i = 0; i <= 3; ++i) {
        int cellId = cellIds[i];
        if (cellId != -1) {
            staticCells[cellId].push_back(obj);
        }
    }
}

void SpatialHashGrid::insertDynamicObject(GameObject* obj) {
    cellIds = getCellIds(obj);
    for (int i = 0; i <= 3; ++i) {
        int cellId = cellIds[i];
        if (cellId != -1) {
            dynamicCells[cellId].push_back(obj);
        }
    }
}

void SpatialHashGrid::removeObject(GameObject* obj) {
    cellIds = getCellIds(obj);
    for (int i = 0; i <= 3; ++i) {
        int cellId = cellIds[i];
        if (cellId != -1) {
            dynamicCells[cellId].erase(std::remove(dynamicCells[cellId].begin(), dynamicCells[cellId].end(), obj), dynamicCells[cellId].end());
            staticCells[cellId].erase(std::remove(staticCells[cellId].begin(), staticCells[cellId].end(), obj), staticCells[cellId].end());
        }
    }
}

void SpatialHashGrid::clearDynamicCells(GameObject* obj) {
    for (int i = 0; i < dynamicCells.size(); ++i) {
        dynamicCells[i].clear();
    }
}

std::vector<GameObject*> SpatialHashGrid::getPotentialColliders(GameObject* obj) {
    foundObjects.clear();
    cellIds = getCellIds(obj);
    for (int i = 0; i <= 3; ++i) {
        int cellId = cellIds[i];
        if (cellId != -1) {
            for (GameObject* collider : dynamicCells[cellId]) {
                if (std::find(foundObjects.begin(), foundObjects.end(), collider) == foundObjects.end()) {
                    foundObjects.push_back(collider);
                }
            }
            for (GameObject* collider : staticCells[cellId]) {
                if (std::find(foundObjects.begin(), foundObjects.end(), collider) == foundObjects.end()) {
                    foundObjects.push_back(collider);
                }
            }
        }
    }
    return foundObjects;
}

std::vector<int> SpatialHashGrid::getCellIds(GameObject* obj) {
    std::vector<int> cellIds(4, -1);
    return cellIds;
}
