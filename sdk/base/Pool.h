#ifndef POOL_H
#define POOL_H

#include <functional>

template <typename T>
class PoolObjectFactory {
public:
    virtual T* createObject() = 0;
};

template <typename T>
class Pool {
public:
    Pool(PoolObjectFactory<T>* factory, int size);
    ~Pool();
    T* newObject();
    void free(T* object);
};

#endif // POOL_H
