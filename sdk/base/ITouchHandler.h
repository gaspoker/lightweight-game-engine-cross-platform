#ifndef ITOUCHHANDLER_H
#define ITOUCHHANDLER_H

#include <vector>

class ITouchHandler {
public:
    virtual bool isTouchDown(int pointer) = 0;
    virtual int getTouchX(int pointer) = 0;
    virtual int getTouchY(int pointer) = 0;
    virtual std::vector<TouchEvent> getTouchEvents() = 0;
};

#endif // ITOUCHHANDLER_H
