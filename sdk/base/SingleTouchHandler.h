#ifndef SINGLETOUCHHANDLER_H
#define SINGLETOUCHHANDLER_H

#include <View.h>
#include <MotionEvent.h>
#include <IInput.h>

class SingleTouchHandler : public ITouchHandler {
private:
    bool isTouched;
    int touchX;
    int touchY;
    Pool<TouchEvent>* touchEventPool;
    std::vector<TouchEvent*> touchEventsBuffer;
    std::vector<TouchEvent*> touchEvents;

public:
    SingleTouchHandler(View* view, float scaleX, float scaleY);
    ~SingleTouchHandler();
    virtual bool onTouch(View* v, MotionEvent* event) override;
    virtual bool isTouchDown(int pointer) override;
    virtual int getTouchX(int pointer) override;
    virtual int getTouchY(int pointer) override;
    virtual std::vector<TouchEvent*> getTouchEvents() override;
};

#endif // SINGLETOUCHHANDLER_H
