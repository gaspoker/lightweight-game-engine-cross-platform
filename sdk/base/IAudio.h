#ifndef IAUDIO_H
#define IAUDIO_H

#include <string>

class IMusic;
class ISound;

class IAudio {
public:
    virtual IMusic* newMusic(std::string filename) = 0;
    virtual ISound* newSound(std::string filename) = 0;
    // virtual ISound* newSound(std::string filename, long duration) = 0;

    virtual void stopAll() = 0;
    virtual void pause() = 0;
    virtual void resume() = 0;
    virtual bool isPlayingMusic() = 0;
};

#endif // IAUDIO_H
