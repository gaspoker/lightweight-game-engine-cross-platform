#ifndef SPATIALHASHGRID_H
#define SPATIALHASHGRID_H

#include <vector>

class GameObject {
};

class SpatialHashGrid {
private:
    std::vector<GameObject*> dynamicCells;
    std::vector<GameObject*> staticCells;
    int cellsPerRow;
    int cellsPerCol;
    float cellSize;
    std::vector<int> cellIds;
    std::vector<GameObject*> foundObjects;

public:
    SpatialHashGrid(float worldWidth, float worldHeight, float cellSize);

    void insertStaticObject(GameObject* obj);

    void insertDynamicObject(GameObject* obj);

    void removeObject(GameObject* obj);

    void clearDynamicCells(GameObject* obj);

    std::vector<GameObject*> getPotentialColliders(GameObject* obj);

    std::vector<int> getCellIds(GameObject* obj);
};

#endif // SPATIALHASHGRID_H
