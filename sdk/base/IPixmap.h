#ifndef IPIXMAP_H
#define IPIXMAP_H

#include "IGraphics.h"

class IPixmap {
public:
    virtual int getWidth() = 0;
    virtual int getHeight() = 0;
    virtual IGraphics::PixmapFormat getFormat() = 0;
    virtual void dispose() = 0;
};

#endif // IPIXMAP_H
