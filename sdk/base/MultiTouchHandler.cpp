#include "MultiTouchHandler.h"

MultiTouchHandler::MultiTouchHandler(View* view, float scaleX, float scaleY) : scaleX(scaleX), scaleY(scaleY) {
    PoolObjectFactory<TouchEvent> factory = []() { return new TouchEvent(); };
    this->touchEventPool = new Pool<TouchEvent>(factory, 100);
    this->slop = ViewConfiguration::get(view->getContext())->getScaledTouchSlop();
    this->startDragging = 0;
    view->setOnTouchListener(this);
}

bool MultiTouchHandler::onTouch(View* v, MotionEvent* event) {
    synchronized(this) {
        int action = event->getAction() & MotionEvent::ACTION_MASK;
        int pointerIndex = (event->getAction() & MotionEvent::ACTION_POINTER_ID_MASK) >> MotionEvent::ACTION_POINTER_ID_SHIFT;
        int pointerCount = event->getPointerCount();
        TouchEvent* touchEvent;
        for (int i = 0; i < MAX_TOUCHPOINTS; i++) {
            if (i >= pointerCount) {
                isTouched[i] = false;
                id[i] = -1;
                continue;
            }
            int pointerId = event->getPointerId(i);
            if (event->getAction() != MotionEvent::ACTION_MOVE && i != pointerIndex) {
                continue;
            }
            switch (action) {
                case MotionEvent::ACTION_DOWN:
                case MotionEvent::ACTION_POINTER_DOWN:
                    touchEvent = touchEventPool->newObject();
                    touchEvent->type = TouchEvent::TOUCH_DOWN;
                    touchEvent->dragDirection = TouchEvent::DRAG_NONE;
                    touchEvent->dragDuration = 0;
                    touchEvent->pointer = pointerId;
                    touchEvent->x = touchX[i] = (int) (event->getX(i) * scaleX);
                    touchEvent->y = touchY[i] = (int) (event->getY(i) * scaleY);
                    isTouched[i] = true;
                    id[i] = pointerId;
                    touchEventsBuffer.push_back(touchEvent);
                    initialX = touchX[i];
                    initialY = touchY[i];
                    startDragging = System.nanoTime();
                    break;
                case MotionEvent::ACTION_UP:
                case MotionEvent::ACTION_POINTER_UP:
                case MotionEvent::ACTION_CANCEL:
                    touchEvent = touchEventPool->newObject();
                    touchEvent->type = TouchEvent::TOUCH_UP;
                    touchEvent->dragDirection = TouchEvent::DRAG_NONE;
                    touchEvent->dragDuration = (System.nanoTime() - startDragging) / 1000000;
                    touchEvent->pointer = pointerId;
                    touchEvent->x = touchX[i] = (int) (event->getX(i) * scaleX);
                    touchEvent->y = touchY[i] = (int) (event->getY(i) * scaleY);
                    isTouched[i] = false;
                    id[i] = -1;
                    touchEventsBuffer.push_back(touchEvent);
                    break;
                case MotionEvent::ACTION_MOVE:
                    touchEvent = touchEventPool->newObject();
                    touchEvent->type = TouchEvent::TOUCH_DRAGGED;
                    touchEvent->dragDuration = (System.nanoTime() - startDragging) / 1000000;
                    int currentX = (int) (event->getX(i) * scaleX);
                    int currentY = (int) (event->getY(i) * scaleY);
                    int offsetX = currentX - initialX;
                    int offsetY = currentY - initialY;
                    if (Math.abs(offsetX) > slop) {
                        if (offsetX > TouchEvent::DEFAULT_THRESHOLD) {
                            touchEvent->dragDirection = TouchEvent::DRAG_RIGHT;
                        } else if (offsetX < -TouchEvent::DEFAULT_THRESHOLD) {
                            touchEvent->dragDirection = TouchEvent::DRAG_LEFT;
                        }
                    }
                    if (Math.abs(offsetY) > slop) {
                        if (offsetY > TouchEvent::DEFAULT_THRESHOLD) {
                            touchEvent->dragDirection = TouchEvent::DRAG_DOWN;
                        } else if (offsetY < -TouchEvent::DEFAULT_THRESHOLD) {
                            touchEvent->dragDirection = TouchEvent::DRAG_UP;
                        }
                    }
                    touchEvent->pointer = pointerId;
                    touchEvent->x = touchX[i] = (int) (event->getX(i) * scaleX);
                    touchEvent->y = touchY[i] = (int) (event->getY(i) * scaleY);
                    isTouched[i] = true;
                    id[i] = pointerId;
                    touchEventsBuffer.push_back(touchEvent);
                    break;
            }
        }
        return true;
    }
}
