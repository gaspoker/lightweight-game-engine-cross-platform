#ifndef INPUT_H
#define INPUT_H

#include "IInput.h"
#include "AccelerometerHandler.h"
#include "KeyboardHandler.h"
#include "ITouchHandler.h"
#include "SingleTouchHandler.h"
#include "MultiTouchHandler.h"
#include <vector>

class Context;
class View;

class Input : public IInput {
private:
    AccelerometerHandler* accelHandler;
    KeyboardHandler* keyHandler;
    ITouchHandler* touchHandler;

public:
    Input(Context* context, View* view, float scaleX, float scaleY) {
        accelHandler = new AccelerometerHandler(context);
        keyHandler = new KeyboardHandler(view);

        if (VERSION::SDK < 5) 
            touchHandler = new SingleTouchHandler(view, scaleX, scaleY);
        else
            touchHandler = new MultiTouchHandler(view, scaleX, scaleY);
    }

    bool isKeyPressed(int keyCode) override {
        return keyHandler->isKeyPressed(keyCode);
    }

    bool isTouchDown(int pointer) override {
        return touchHandler->isTouchDown(pointer);
    }

    int getTouchX(int pointer) override {
        return touchHandler->getTouchX(pointer);
    }

    int getTouchY(int pointer) override {
        return touchHandler->getTouchY(pointer);
    }

    float getAccelX() override {
        return accelHandler->getAccelX();
    }

    float getAccelY() override {
        return accelHandler->getAccelY();
    }

    float getAccelZ() override {
        return accelHandler->getAccelZ();
    }

    std::vector<TouchEvent> getTouchEvents() override {
        return touchHandler->getTouchEvents();
    }

    std::vector<KeyEvent> getKeyEvents() override {
        return keyHandler->getKeyEvents();
    }
};

#endif // INPUT_H
