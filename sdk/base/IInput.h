#ifndef IINPUT_H
#define IINPUT_H

#include <vector>

class IInput {
public:
    class KeyEvent {
    public:
        static const int KEY_DOWN = 0;
        static const int KEY_UP = 1;

        int type;
        int keyCode;
        char keyChar;

        std::string toString() {
            std::stringstream builder;
            if (type == KEY_DOWN)
                builder << "key down, ";
            else
                builder << "key up, ";
            builder << keyCode << "," << keyChar;
            return builder.str();
        }
    };

    class TouchEvent {
    public:
        static const int TOUCH_DOWN = 0;
        static const int TOUCH_UP = 1;
        static const int TOUCH_DRAGGED = 2;
        static const int TOUCH_OUT = 3;

        static const int DEFAULT_THRESHOLD = 20; // Better to be something in dps.
        static const int DRAG_NONE = 0;
        static const int DRAG_DOWN = 1;
        static const int DRAG_UP = 2;
        static const int DRAG_LEFT = 3;
        static const int DRAG_RIGHT = 4;

        int type;
        int dragDirection;
        long dragDuration;
        int x, y;
        int pointer;

        std::string toString() {
            std::stringstream builder;

            // Type
            if (type == TOUCH_DOWN)
                builder << "touch down, ";
            else if (type == TOUCH_UP)
                builder << "touch up, ";
            else if (type == TOUCH_DRAGGED)
                builder << "touch dragged, ";
            else
                builder << "touch out, ";

            // Drag Direction
            if (dragDirection == DRAG_DOWN)
                builder << "drag down, ";
            else if (dragDirection == DRAG_UP)
                builder << "drag up, ";
            else if (dragDirection == DRAG_LEFT)
                builder << "drag left, ";
            else if (dragDirection == DRAG_RIGHT)
                builder << "drag right, ";
            else
                builder << "no drag, ";

            builder << pointer << "," << x << "," << y;
            return builder.str();
        }
    };

    virtual bool isKeyPressed(int keyCode) = 0;
    virtual bool isTouchDown(int pointer) = 0;
    virtual int getTouchX(int pointer) = 0;
    virtual int getTouchY(int pointer) = 0;
    virtual float getAccelX() = 0;
    virtual float getAccelY() = 0;
    virtual float getAccelZ() = 0;
    virtual std::vector<KeyEvent> getKeyEvents() = 0;
    virtual std::vector<TouchEvent> getTouchEvents() = 0;
};

#endif // IINPUT_H
