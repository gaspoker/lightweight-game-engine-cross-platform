#ifndef IGRAPHICS_H
#define IGRAPHICS_H

#include <string>

class IPixmap;

class IGraphics {
public:
    enum PixmapFormat {
        ARGB8888,
        ARGB4444,
        RGB565
    };

    virtual IPixmap* newPixmap(std::string fileName, PixmapFormat format) = 0;
    virtual void clear(int color) = 0;
    virtual void drawPixel(float x, float y, int color, float alpha) = 0;
    virtual void drawLine(float x, float y, float x2, float y2, int color, float alpha) = 0;
    virtual void drawRect(float x, float y, float width, float height, int color, float alpha) = 0;
    virtual void drawPixmap(IPixmap* pixmap, int x, int y, int srcX, int srcY, int srcWidth, int srcHeight) = 0;
    virtual void drawPixmap(IPixmap* pixmap, int x, int y) = 0;
    virtual int getWidth() = 0;
    virtual int getHeight() = 0;
};

#endif // IGRAPHICS_H
