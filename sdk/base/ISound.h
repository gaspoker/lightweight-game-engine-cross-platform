#ifndef ISOUND_H
#define ISOUND_H

class ISound {
public:
    virtual void play(float volume, bool loop) = 0;
    virtual void pause() = 0;
    virtual void stop() = 0;
    //virtual void pauseAll() = 0; // Comentado porque no está presente en la interfaz original
    virtual void dispose() = 0;
};

#endif // ISOUND_H
