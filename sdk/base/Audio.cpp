#include <iostream>
#include <vector>
#include <string>

#include "AssetFileDescriptor.h" // TODO
#include "ISound.h"
#include "IMusic.h"

// Simulación de la clase AudioManager
class AudioManager {
public:
    static const int STREAM_MUSIC = 3;
};

// Simulación de la clase SoundPool
class SoundPool {
public:
    SoundPool(int maxStreams, int streamType, int srcQuality) {
        // TODO
    }

    int load(AssetFileDescriptor assetDescriptor, int priority) {
        // TODO
        return 0; // TODO
    }

    void autoPause() {
        // TODO
    }

    void autoResume() {
        // TODO
    }
};

// Simulación de la clase MediaPlayer
class MediaPlayer {
public:
    void setVolume(int leftVolume, int rightVolume) {
        // TODO
    }

    void start() {
        // TODO
    }

    bool isPlaying() {
        // TODO
        return false; // TODO
    }

    void stop() {
        // TODO
    }
};

// Implementación de la clase Audio
class Audio {
    AssetManager assets;
    SoundPool soundPool;
    std::vector<MediaPlayer*> musicPool;

public:
    Audio(Activity activity) {
        activity.setVolumeControlStream(AudioManager::STREAM_MUSIC);
        this->assets = activity.getAssets();
        this->soundPool = SoundPool(30, AudioManager::STREAM_MUSIC, 0);
    }

    IMusic* newMusic(std::string filename) {
        try {
            AssetFileDescriptor assetDescriptor;
            MediaPlayer* mediaPlayer = new MediaPlayer();
            musicPool.push_back(mediaPlayer);
        } catch (std::exception& e) {
            throw std::runtime_error("Couldn't load music '" + filename + "'");
        }
        return nullptr; // TODO
    }

    ISound* newSound(std::string filename) {
        try {
            AssetFileDescriptor assetDescriptor;
            int soundId = soundPool.load(assetDescriptor, 0);
        } catch (std::exception& e) {
            throw std::runtime_error("Couldn't load sound '" + filename + "'");
        }
        return nullptr; // TODO
    }

    void pause() {
        this->soundPool.autoPause();
        for (auto music : musicPool) {
            music->setVolume(0, 0);
        }
    }

    void stopAll() {
        this->soundPool.autoPause();
        for (auto music : musicPool) {
            if (music->isPlaying()) {
                music->stop();
            }
        }
    }

    void resume() {
        this->soundPool.autoResume();
        /*for (auto mp : musicPool) {
            mp->start();
        }*/
    }

    bool isPlayingMusic() {
        for (auto music : musicPool) {
            if (music->isPlaying()) {
                return true;
            }
        }
        return false;
    }
};
