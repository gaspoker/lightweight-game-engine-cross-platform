#ifndef IGAME_H
#define IGAME_H

class IInput;
class IFileIO;
class IGraphics;
class IAudio;
class Scene;

class IGame {
public:
    virtual IInput* getInput() = 0;
    virtual IFileIO* getFileIO() = 0;
    virtual IGraphics* getGraphics() = 0;
    virtual IAudio* getAudio() = 0;

    virtual void setScene(Scene* scene) = 0;
    virtual Scene* getScene() = 0;
    virtual Scene* getMainScene() = 0;
    virtual void loadAssets() = 0;
};

#endif // IGAME_H
