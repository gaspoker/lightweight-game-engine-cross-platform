#include "CompassHandler.h"

#include "Context.h"
#include "Sensor.h"
#include "SensorEvent.h"
#include "SensorManager.h"

CompassHandler::CompassHandler(Context* context) {
    SensorManager* manager = context->getSystemService(Context::SENSOR_SERVICE);
    if (manager->getSensorList(Sensor::TYPE_ORIENTATION).size() != 0) {
        Sensor* compass = manager->getDefaultSensor(Sensor::TYPE_ORIENTATION);
        manager->registerListener(this, compass, SensorManager::SENSOR_DELAY_GAME);
    }
}

void CompassHandler::onAccuracyChanged(Sensor* sensor, int accuracy) {
    // nothing to do here
}

void CompassHandler::onSensorChanged(SensorEvent* event) {
    yaw = event->values[0];
    pitch = event->values[1];
    roll = event->values[2];
}

float CompassHandler::getYaw() {
    return yaw;
}

float CompassHandler::getPitch() {
    return pitch;
}

float CompassHandler::getRoll() {
    return roll;
}
