#include <chrono>
#include <iostream>

class FPSCounter {
private:
    std::chrono::steady_clock::time_point startTime;
    int frames;

public:
    FPSCounter() : startTime(std::chrono::steady_clock::now()), frames(0) {}

    void logFrame() {
        frames++;
        auto currentTime = std::chrono::steady_clock::now();
        auto elapsedTime = std::chrono::duration_cast<std::chrono::nanoseconds>(currentTime - startTime).count();

        if (elapsedTime >= 1000000000) {
            std::cout << "fps: " << frames << std::endl;
            frames = 0;
            startTime = currentTime;
        }
    }
};
