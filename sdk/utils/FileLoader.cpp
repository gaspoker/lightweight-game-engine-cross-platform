#include <fstream>
#include <sstream>
#include <vector>
#include <stdexcept>
#include <iostream>
#include <json/json.h> // C++ JSON library, e.g., JsonCpp

#include "geometry/Vector3D"
#include "opengl/GLGame"
#include "opengl/GLVertices3D"

class FileLoader {
public:
    static GLVertices3D loadOBJ(GLGame& game, const std::string& file) {
        std::ifstream in(file);
        if (!in.is_open()) {
            throw std::runtime_error("Unable to open file: " + file);
        }

        std::vector<float> vertices;
        std::vector<float> normals;
        std::vector<float> uv;
        std::vector<int> facesVerts;
        std::vector<int> facesNormals;
        std::vector<int> facesUV;

        int numVertices = 0, numNormals = 0, numUV = 0, numFaces = 0;
        int vertexIndex = 0, normalIndex = 0, uvIndex = 0, faceIndex = 0;

        std::string line;
        while (std::getline(in, line)) {
            std::istringstream iss(line);
            std::string token;
            iss >> token;

            if (token == "v") {
                float x, y, z;
                iss >> x >> y >> z;
                vertices.push_back(x);
                vertices.push_back(y);
                vertices.push_back(z);
                numVertices++;
            } else if (token == "vn") {
                float x, y, z;
                iss >> x >> y >> z;
                normals.push_back(x);
                normals.push_back(y);
                normals.push_back(z);
                numNormals++;
            } else if (token == "vt") {
                float u, v;
                iss >> u >> v;
                uv.push_back(u);
                uv.push_back(v);
                numUV++;
            } else if (token == "f") {
                std::string part;
                for (int i = 0; i < 3; ++i) {
                    iss >> part;
                    size_t pos = part.find('/');
                    facesVerts.push_back(std::stoi(part.substr(0, pos)) - 1);
                    if (pos != std::string::npos) {
                        size_t nextPos = part.find('/', pos + 1);
                        if (nextPos != std::string::npos) {
                            facesUV.push_back(std::stoi(part.substr(pos + 1, nextPos - pos - 1)) - 1);
                            facesNormals.push_back(std::stoi(part.substr(nextPos + 1)) - 1);
                        } else {
                            facesNormals.push_back(std::stoi(part.substr(pos + 1)) - 1);
                        }
                    }
                }
                numFaces++;
            }
        }

        std::vector<float> verts;
        for (int i = 0; i < numFaces * 3; ++i) {
            int vertexIdx = facesVerts[i] * 3;
            verts.push_back(vertices[vertexIdx]);
            verts.push_back(vertices[vertexIdx + 1]);
            verts.push_back(vertices[vertexIdx + 2]);

            if (numUV > 0) {
                int uvIdx = facesUV[i] * 2;
                verts.push_back(uv[uvIdx]);
                verts.push_back(1 - uv[uvIdx + 1]);
            }

            if (numNormals > 0) {
                int normalIdx = facesNormals[i] * 3;
                verts.push_back(normals[normalIdx]);
                verts.push_back(normals[normalIdx + 1]);
                verts.push_back(normals[normalIdx + 2]);
            }
        }

        GLVertices3D model(game, numFaces * 3, 0, false, numUV > 0, numNormals > 0);
        model.setVertices(verts.data(), 0, verts.size());
        return model;
    }

    static Json::Value loadJSON(GLGame& game, const std::string& file) {
        std::ifstream ifs(file);
        if (!ifs.is_open()) {
            throw std::runtime_error("Unable to open file: " + file);
        }

        Json::Reader reader;
        Json::Value root;
        if (!reader.parse(ifs, root)) {
            throw std::runtime_error("Failed to parse JSON from file: " + file);
        }

        return root;
    }
};
