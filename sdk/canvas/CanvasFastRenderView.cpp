#ifndef CANVAS_FAST_RENDER_VIEW_H
#define CANVAS_FAST_RENDER_VIEW_H

#include <android/native_window.h>
#include <android/native_window_jni.h>

class CanvasFastRenderView {
public:
    CanvasFastRenderView(CanvasGame* game, ANativeWindow* window, jobject bitmap)
        : game(game), window(window), framebuffer(nullptr), running(false) {
        JNIEnv* env;
        jvm->AttachCurrentThread(&env, nullptr);

        jclass bitmapClass = env->FindClass("android/graphics/Bitmap");
        jmethodID getBufferMethod = env->GetMethodID(bitmapClass, "getBuffer", "()Ljava/nio/ByteBuffer;");
        jobject byteBuffer = env->CallObjectMethod(bitmap, getBufferMethod);
        framebuffer = static_cast<uint32_t*>(env->GetDirectBufferAddress(byteBuffer));
    }

    ~CanvasFastRenderView() {}

    void resume() {
        running = true;
        renderThread = std::thread(&CanvasFastRenderView::run, this);
    }

    void run() {
        while (running) {
            if (!window || !framebuffer) continue;

            ANativeWindow_Buffer buffer;
            if (ANativeWindow_lock(window, &buffer, nullptr) < 0) continue;

            game->getScene()->update(deltaTime);
            game->getScene()->present(deltaTime);

            int32_t* pixels = static_cast<int32_t*>(buffer.bits);
            for (int y = 0; y < buffer.height; ++y) {
                for (int x = 0; x < buffer.width; ++x) {
                    pixels[x] = framebuffer[x + y * buffer.stride];
                }
                pixels += buffer.stride;
            }

            ANativeWindow_unlockAndPost(window);
        }
    }

    void pause() {
        running = false;
        if (renderThread.joinable()) renderThread.join();
    }

private:
    CanvasGame* game;
    ANativeWindow* window;
    uint32_t* framebuffer;
    std::thread renderThread;
    bool running;
};

#endif // CANVAS_FAST_RENDER_VIEW_H
