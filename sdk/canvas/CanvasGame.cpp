#ifndef CANVAS_GAME_H
#define CANVAS_GAME_H

#include <android_native_app_glue.h>
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>
#include "CanvasFastRenderView.h"
#include "CanvasGraphics.h"
#include "base/IAudio.h"
#include "base/IInput.h"
#include "base/IFileIO.h"
#include "base/IGame.h"
#include "core/Scene.h"

class CanvasGame : public IGame {
public:
    CanvasGame(android_app* app) 
        : app(app), renderView(nullptr), graphics(nullptr), audio(nullptr), 
          input(nullptr), fileIO(nullptr), scene(nullptr), wakeLock(nullptr),
          width(0), height(0) {}

    virtual ~CanvasGame() {}

    virtual void onCreate() {
        // Initialize necessary components
        // Get display dimensions and calculate scale factors
        // Create frame buffer bitmap
        
        // Initialize render view
        renderView = new CanvasFastRenderView(this, frameBufferBitmap);
        
        // Initialize graphics, audio, input, fileIO
        graphics = new CanvasGraphics(app->activity->assetManager, frameBufferBitmap);
        audio = new Audio(app);
        fileIO = new FileIO(app->activity);
        input = new Input(app, renderView, scaleX, scaleY);

        // Get PowerManager and acquire wake lock
        // Set content view to renderView
    }

    virtual void onResume() {
        // Acquire wake lock
        // Resume scene and render view
    }

    virtual void onPause() {
        // Release wake lock
        // Pause render view and scene
        // Dispose scene if activity is finishing
    }

    virtual IInput* getInput() { return input; }
    virtual IFileIO* getFileIO() { return fileIO; }
    virtual IGraphics* getGraphics() { return graphics; }
    virtual IAudio* getAudio() { return audio; }
    virtual void setScene(Scene* newScene) { /* Set new scene */ }
    virtual Scene* getScene() { return scene; }

private:
    android_app* app;
    CanvasFastRenderView* renderView;
    IGraphics* graphics;
    IAudio* audio;
    IInput* input;
    IFileIO* fileIO;
    Scene* scene;
    ANativeActivity* activity;
    ANativeWindow* window;
    ANativeWindow_Buffer buffer;
    WakeLock* wakeLock;
    float width;
    float height;
    Bitmap* frameBufferBitmap;
    float scaleX;
    float scaleY;
};

#endif // CANVAS_GAME_H
