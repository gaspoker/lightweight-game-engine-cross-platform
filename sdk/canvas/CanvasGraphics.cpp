#include <android/bitmap.h> // TODO
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>
#include <jni.h>

class CanvasGraphics {
private:
    AAssetManager* assets;
    jobject frameBuffer;
    jobject canvas;
    jobject paint;
    jobject srcRect;
    jobject dstRect;

public:
    CanvasGraphics(AAssetManager* assetManager, jobject frameBuffer) {
        this->assets = assetManager;
        this->frameBuffer = frameBuffer;

        // Initialize canvas and paint objects using JNI calls
        JNIEnv* env;
        // Attach JNI environment
        JavaVM* jvm;
        env->GetJavaVM(&jvm);
        jvm->AttachCurrentThread(&env, NULL);

        // Get class and method IDs
        jclass canvasClass = env->FindClass("android/graphics/Canvas");
        jclass paintClass = env->FindClass("android/graphics/Paint");
        jclass rectClass = env->FindClass("android/graphics/Rect");
        jmethodID constructorCanvas = env->GetMethodID(canvasClass, "<init>", "(Landroid/graphics/Bitmap;)V");
        jmethodID constructorPaint = env->GetMethodID(paintClass, "<init>", "()V");
        jmethodID constructorRect = env->GetMethodID(rectClass, "<init>", "()V");

        // Create canvas, paint, srcRect, and dstRect objects
        this->canvas = env->NewObject(canvasClass, constructorCanvas, this->frameBuffer);
        this->paint = env->NewObject(paintClass, constructorPaint);
        this->srcRect = env->NewObject(rectClass, constructorRect);
        this->dstRect = env->NewObject(rectClass, constructorRect);

        // Detach JNI environment
        jvm->DetachCurrentThread();
    }

    ~CanvasGraphics() {
        // Delete local references
        JNIEnv* env;
        JavaVM* jvm;
        env->GetJavaVM(&jvm);
        jvm->AttachCurrentThread(&env, NULL);
        env->DeleteLocalRef(this->canvas);
        env->DeleteLocalRef(this->paint);
        env->DeleteLocalRef(this->srcRect);
        env->DeleteLocalRef(this->dstRect);
        jvm->DetachCurrentThread();
    }

    void clear(int color) {
        JNIEnv* env;
        JavaVM* jvm;
        env->GetJavaVM(&jvm);
        jvm->AttachCurrentThread(&env, NULL);

        // Get the clear method ID
        jclass canvasClass = env->FindClass("android/graphics/Canvas");
        jmethodID clearMethod = env->GetMethodID(canvasClass, "drawRGB", "(III)V");

        // Call the clear method
        env->CallVoidMethod(this->canvas, clearMethod, (color & 0xff0000) >> 16, (color & 0xff00) >> 8, (color & 0xff));

        // Detach JNI environment
        jvm->DetachCurrentThread();
    }

    void drawPixel(float x, float y, int color, float alpha) {
        JNIEnv* env;
        JavaVM* jvm;
        env->GetJavaVM(&jvm);
        jvm->AttachCurrentThread(&env, NULL);

        // Get the drawPoint method ID
        jclass canvasClass = env->FindClass("android/graphics/Canvas");
        jmethodID drawPointMethod = env->GetMethodID(canvasClass, "drawPoint", "(FFLandroid/graphics/Paint;)V");

        // Set paint color
        jmethodID setColorMethod = env->GetMethodID(env->GetObjectClass(this->paint), "setColor", "(I)V");
        env->CallVoidMethod(this->paint, setColorMethod, color);

        // Call the drawPoint method
        env->CallVoidMethod(this->canvas, drawPointMethod, x, y, this->paint);

        // Detach JNI environment
        jvm->DetachCurrentThread();
    }

    void drawLine(float x, float y, float x2, float y2, int color, float alpha) {
        JNIEnv* env;
        JavaVM* jvm;
        env->GetJavaVM(&jvm);
        jvm->AttachCurrentThread(&env, NULL);

        // Get the drawLine method ID
        jclass canvasClass = env->FindClass("android/graphics/Canvas");
        jmethodID drawLineMethod = env->GetMethodID(canvasClass, "drawLine", "(FFFFLandroid/graphics/Paint;)V");

        // Set paint color
        jmethodID setColorMethod = env->GetMethodID(env->GetObjectClass(this->paint), "setColor", "(I)V");
        env->CallVoidMethod(this->paint, setColorMethod, color);

        // Call the drawLine method
        env->CallVoidMethod(this->canvas, drawLineMethod, x, y, x2, y2, this->paint);

        // Detach JNI environment
        jvm->DetachCurrentThread();
    }

    void drawRect(float x, float y, float width, float height, int color, float alpha) {
        JNIEnv* env;
        JavaVM* jvm;
        env->GetJavaVM(&jvm);
        jvm->AttachCurrentThread(&env, NULL);

        // Get the drawRect method ID
        jclass canvasClass = env->FindClass("android/graphics/Canvas");
        jmethodID drawRectMethod = env->GetMethodID(canvasClass, "drawRect", "(FFFFLandroid/graphics/Paint;)V");

        // Set paint color and style
        jmethodID setColorMethod = env->GetMethodID(env->GetObjectClass(this->paint), "setColor", "(I)V");
        jmethodID setStyleMethod = env->GetMethodID(env->GetObjectClass(this->paint), "setStyle", "(Landroid/graphics/Paint$Style;)V");
        jclass styleClass = env->FindClass("android/graphics/Paint$Style");
        jfieldID fillField = env->GetStaticFieldID(styleClass, "FILL", "Landroid/graphics/Paint$Style;");
        jobject fillStyle = env->GetStaticObjectField(styleClass, fillField);
        env->CallVoidMethod(this->paint, setColorMethod, color);
        env->CallVoidMethod(this->paint, setStyleMethod, fillStyle);

        // Call the drawRect method
        env->CallVoidMethod(this->canvas, drawRectMethod, x, y, x + width - 1, y + height - 1, this->paint);

        // Detach JNI environment
        jvm->DetachCurrentThread();
    }

    void drawPixmap(jobject pixmap, int x, int y, int srcX, int srcY, int srcWidth, int srcHeight) {
        JNIEnv* env;
        JavaVM* jvm;
        env->GetJavaVM(&jvm);
        jvm->AttachCurrentThread(&env, NULL);

        // Get the drawBitmap method ID
        jclass canvasClass = env->FindClass("android/graphics/Canvas");
        jmethodID drawBitmapMethod = env->GetMethodID(canvasClass, "drawBitmap", "(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V");

        // Set source and destination rects
        jfieldID leftField = env->GetFieldID(env->GetObjectClass(this->srcRect), "left", "I");
        jfieldID topField = env->GetFieldID(env->GetObjectClass(this->srcRect), "top", "I");
        jfieldID rightField = env->GetFieldID(env->GetObjectClass(this->srcRect), "right", "I");
        jfieldID bottomField = env->GetFieldID(env->GetObjectClass(this->srcRect), "bottom", "I");
        env->SetIntField(this->srcRect, leftField, srcX);
        env->SetIntField(this->srcRect, topField, srcY);
        env->SetIntField(this->srcRect, rightField, srcX + srcWidth - 1);
        env->SetIntField(this->srcRect, bottomField, srcY + srcHeight - 1);
        env->SetIntField(this->dstRect, leftField, x);
        env->SetIntField(this->dstRect, topField, y);
        env->SetIntField(this->dstRect, rightField, x + srcWidth - 1);
        env->SetIntField(this->dstRect, bottomField, y + srcHeight - 1);

        // Call the drawBitmap method
        env->CallVoidMethod(this->canvas, drawBitmapMethod, pixmap, this->srcRect, this->dstRect, this->paint);

        // Detach JNI environment
        jvm->DetachCurrentThread();
    }

    int getWidth() {
        JNIEnv* env;
        JavaVM* jvm;
        env->GetJavaVM(&jvm);
        jvm->AttachCurrentThread(&env, NULL);

        // Get the getWidth method ID
        jclass bitmapClass = env->FindClass("android/graphics/Bitmap");
        jmethodID getWidthMethod = env->GetMethodID(bitmapClass, "getWidth", "()I");

        // Call the getWidth method
        int width = env->CallIntMethod(this->frameBuffer, getWidthMethod);

        // Detach JNI environment
        jvm->DetachCurrentThread();

        return width;
    }

    int getHeight() {
        JNIEnv* env;
        JavaVM* jvm;
        env->GetJavaVM(&jvm);
        jvm->AttachCurrentThread(&env, NULL);

        // Get the getHeight method ID
        jclass bitmapClass = env->FindClass("android/graphics/Bitmap");
        jmethodID getHeightMethod = env->GetMethodID(bitmapClass, "getHeight", "()I");

        // Call the getHeight method
        int height = env->CallIntMethod(this->frameBuffer, getHeightMethod);

        // Detach JNI environment
        jvm->DetachCurrentThread();

        return height;
    }
};
